package com.iuvo.utils

import android.content.Context
import android.content.SharedPreferences
import android.content.res.Resources
import android.view.View
import android.widget.Button
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.iuvo.models.Location
import com.iuvo.models.UserData

/**
 * Created by apjoex on 25/12/2017.
 */
class BaseUtil(val context : Context) {


    companion object {
        private const val PREFERENCE_FILE: String = "com.iuvo"
        private const val LOOGED_IN_USER: String = "LOOGED_IN_USER"
        private const val LOGGED_IN: String = "LOGGED_IN"
        private const val CUSTOM_LOCATION: String = "CUSTOM_LOCATION"
        private const val CUSTOM_LOCATION_ADDESS: String = "CUSTOM_LOCATION_ADDESS"
        private const val SAVED_LOCATION: String = "SAVED_LOCATION"
        private const val FCM_TOKEN: String = "FCM_TOKEN"
    }

    var preference : SharedPreferences = context.getSharedPreferences(PREFERENCE_FILE, Context.MODE_PRIVATE)

    var loggedInUser : UserData
        get() {
            val type = object : TypeToken<UserData>() {}.type
            return Gson().fromJson(preference.getString(LOOGED_IN_USER,""),type)
        }
        set(value) {
            val json = Gson().toJson(value)
            preference.edit().putString(LOOGED_IN_USER, json).apply()
        }

    var savedLocation : Location
        get() {
            val type = object : TypeToken<Location>() {}.type
            return Gson().fromJson(preference.getString(SAVED_LOCATION,""),type)
        }
        set(value) {
            val json = Gson().toJson(value)
            preference.edit().putString(SAVED_LOCATION, json).apply()
        }

    var isLoggedIn: Boolean
        get() = preference.getBoolean(LOGGED_IN, false)
        set(value) = preference.edit().putBoolean(LOGGED_IN, value).apply()

    var hasCustomLocationSet: Boolean
        get() = preference.getBoolean(CUSTOM_LOCATION, false)
        set(value) = preference.edit().putBoolean(CUSTOM_LOCATION, value).apply()

    var customLocationAddress: String
        get() = preference.getString(CUSTOM_LOCATION_ADDESS, "Tap here to set location now")
        set(value) = preference.edit().putString(CUSTOM_LOCATION_ADDESS, value).apply()

    var fcmToken: String
        get() = preference.getString(FCM_TOKEN,"")
        set(value) = preference.edit().putString(FCM_TOKEN, value).apply()

    fun makeFirebasePathFriendly(path: String): String {
        var newPath: String = path.replace(" ", "_")
        newPath = newPath.replace("/", "_")
        return newPath.toLowerCase()
    }

    fun dpToPx(dp: Int) = (dp * Resources.getSystem().displayMetrics.density).toInt()

    fun View.showIf(condition: Boolean){
        if (condition){
            this.visibility = View.VISIBLE
        }else{
            this.visibility = View.GONE
        }
    }
}
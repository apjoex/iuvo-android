package com.iuvo.utils

import com.iuvo.R
import com.iuvo.models.ItemData

/**
 * Created by apjoex on 25/02/2018.
 */
object DataUtils {


    fun getServices () : MutableList<ItemData>{
        val services = mutableListOf<ItemData>()

        services.add(ItemData("Petrol stations", "gas_station", R.drawable.petrol_station, true))
        services.add(ItemData("Lawyers", "lawyer", R.drawable.lawyers, true))
        services.add(ItemData("Publishers", "book_store", R.drawable.publishers, true))
        services.add(ItemData("Banks", "bank", R.drawable.banks, true))
        services.add(ItemData("ATMs", "atm", R.drawable.atm, true))
        services.add(ItemData("Schools", "school", R.drawable.school, true))
        services.add(ItemData("Police posts","police", R.drawable.police_posts, true))
        services.add(ItemData("Hospitals", "hospital",R.drawable.hospital, true))
        services.add(ItemData("Hotels", "lodging",R.drawable.hotels, true))
        services.add(ItemData("Pharmacy", "pharmacy", R.drawable.pharmacy, true))
        services.add(ItemData("Relaxation centers","amusement_park", R.drawable.relaxation, true))
        services.add(ItemData("Shopping malls","shopping_mall", R.drawable.shopping_mall, true))
        services.add(ItemData("Gym/Fitness/Spa","spa", R.drawable.gym, false))
        services.add(ItemData("Art Studios", "art_gallery", R.drawable.art_studio, true))
        services.add(ItemData("Hair Salons", "beauty_salon", R.drawable.hair_salon, false))
        services.add(ItemData("Laundry", "laundry", R.drawable.laundry, false))
        services.add(ItemData("Bars and Cuisines", "bar", R.drawable.bar, true))
        services.add(ItemData("Bakery", "bakery", R.drawable.bakery, true))
        services.add(ItemData("Casino", "casino", R.drawable.casino, true))
        services.add(ItemData("Cafe", "cafe", R.drawable.cafe, true))
        services.add(ItemData("Car wash", "car_wash", R.drawable.car_wash, true))
        services.add(ItemData("Court house", "courthouse", R.drawable.court_house, true))
        services.add(ItemData("Dentist", "dentist", R.drawable.dentist, false))
        services.add(ItemData("Electrician", "electrician", R.drawable.electrician, false))
        services.add(ItemData("Embassy", "embassy", R.drawable.embassy, true))
        services.add(ItemData("Fire station", "fire_station", R.drawable.fire_station, true))
        services.add(ItemData("Florist", "florist", R.drawable.florist, true))
        services.add(ItemData("Insurance agency", "insurance_agency", R.drawable.insurance, true))
        services.add(ItemData("Library", "library", R.drawable.library, true))
        services.add(ItemData("Movie theater", "movie_theater", R.drawable.movie_theater, true))
        services.add(ItemData("Museum", "museum", R.drawable.museum, true))
        services.add(ItemData("Night club", "night_club", R.drawable.night_club, true))
        services.add(ItemData("Painter", "painter", R.drawable.painter, false))
        services.add(ItemData("Park", "park", R.drawable.park, true))
        services.add(ItemData("Post office", "post_office", R.drawable.post_office, true))
        services.add(ItemData("Real estate agency", "real_estate_agency", R.drawable.real_estate, true))
        services.add(ItemData("Restaurant", "restaurant", R.drawable.restaurant, true))
        services.add(ItemData("Stadium", "stadium", R.drawable.statdium, true))
        services.add(ItemData("Travel agency", "travel_agency", R.drawable.travel_agency, false))
        services.add(ItemData("Veterinary care", "veterinary_care", R.drawable.vet, false))
        services.add(ItemData("Zoo", "zoo", R.drawable.zoo, true))
        services.add(ItemData("Airport", "airport", R.drawable.airport,true))

        return services
    }


    fun getRepairs(): MutableList<ItemData> {

        val repairs = mutableListOf<ItemData>()
        repairs.add(ItemData("Automobile","", R.drawable.automobiles, false))
        repairs.add(ItemData("Home electronics","", R.drawable.home_electronics, false))
        repairs.add(ItemData("Petrol engines","", R.drawable.petrol_engines, false))
        repairs.add(ItemData("Diesel engines","", R.drawable.diesel_engines, false))
        repairs.add(ItemData("High voltage electronics","", R.drawable.high_voltage, false))
        repairs.add(ItemData("Mobile gadgets","", R.drawable.mobile_gadgets, false))
        repairs.add(ItemData("Phones and computers","", R.drawable.phones_computers, false))
        repairs.add(ItemData("Plumber","", R.drawable.plumber, false))
        repairs.add(ItemData("Locksmith","", R.drawable.locksmith, false))

        return repairs

    }

    fun getSales(): MutableList<ItemData> {

        val sales = mutableListOf<ItemData>()
        sales.add(ItemData("Auto spare parts", "", R.drawable.auto_spare_parts, false))
        sales.add(ItemData("Household fittings", "", R.drawable.household_fittings, false))
        sales.add(ItemData("Home appliances","", R.drawable.home_appliances, false))
        sales.add(ItemData("Phone and accessories","", R.drawable.phone_accessories, false))
        sales.add(ItemData("Electronic gadgets","", R.drawable.electronic_gadgets, false))
        sales.add(ItemData("Automobiles","", R.drawable.automobile_sales, false))
        sales.add(ItemData("Farm products","", R.drawable.farm_products, false))
        sales.add(ItemData("Cakes and groceries","", R.drawable.cales_groceries, false))
        sales.add(ItemData("Food and consumables", "", R.drawable.food_consumables, false))
        sales.add(ItemData("Boutique and fashion houses","", R.drawable.boutique, false))
        sales.add(ItemData("Household/Office furniture","", R.drawable.household_furniture, false))
        sales.add(ItemData("Bookstore", "", R.drawable.bookstore, false))
        sales.add(ItemData("Liquor store", "", R.drawable.liquor, false))
        sales.add(ItemData("Jewelry store", "", R.drawable.jewelry, false))
        sales.add(ItemData("Pet store", "", R.drawable.pet_store, false))
        sales.add(ItemData("Shoe store", "", R.drawable.shoe_store, false))

        return sales
    }

}
package com.iuvo.adapters

import android.content.Context
import android.content.Intent
import android.support.constraint.ConstraintLayout
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.iuvo.R
import com.iuvo.ui.activities.ChatActivity

class ChannelListAdapter(var context: Context, var channels: MutableList<Pair<String, String>>) : RecyclerView.Adapter<ChannelListAdapter.ViewHolder>() {

    private val inflater = LayoutInflater.from(context)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = inflater.inflate(R.layout.channel_list_item, parent, false)
        return ViewHolder(itemView = view)
    }

    override fun getItemCount() = channels.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        val (channelID, name) = channels[position]

        holder.chatName.text = name

        holder.body.setOnClickListener {
            val intent = Intent(context, ChatActivity::class.java)
            intent.putExtra("channelID", channelID)
            intent.putExtra("name", name)
            intent.putExtra("isForum", false)
            context.startActivity(intent)
        }
    }

    class ViewHolder(itemView : View) : RecyclerView.ViewHolder(itemView) {
        val chatName : TextView = itemView.findViewById(R.id.chatName)
        val body = itemView.findViewById(R.id.body) as ConstraintLayout
    }
}
package com.iuvo.adapters

import android.content.Context
import android.content.Intent
import android.support.constraint.ConstraintLayout
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.iuvo.R
import com.iuvo.ui.activities.ChatActivity


class ForumListAdapter(var context: Context, var forumNames: MutableList<String>) : RecyclerView.Adapter<ForumListAdapter.ViewHolder>() {

    private val inflater = LayoutInflater.from(context)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = inflater.inflate(R.layout.channel_list_item, parent, false)
        return ViewHolder(itemView = view)
    }

    override fun getItemCount() = forumNames.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        holder.chatName.text = forumNames[position]

        holder.chatIcon.setImageResource(R.drawable.ic_forum)

        holder.body.setOnClickListener {
            val intent = Intent(context, ChatActivity::class.java)
            intent.putExtra("name", forumNames[position])
            intent.putExtra("channelID", "")
            intent.putExtra("isForum", true)
            context.startActivity(intent)
        }
    }

    class ViewHolder(itemView : View) : RecyclerView.ViewHolder(itemView) {
        val chatName : TextView = itemView.findViewById(R.id.chatName)
        val body = itemView.findViewById(R.id.body) as ConstraintLayout
        val chatIcon = itemView.findViewById(R.id.chatIcon) as ImageView
    }
}
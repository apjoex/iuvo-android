package com.iuvo.adapters

import android.content.Context
import android.content.Intent
import android.support.v7.widget.CardView
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.iuvo.R
import com.iuvo.models.ItemData
import com.iuvo.ui.activities.RepairsActivity
import com.iuvo.ui.activities.ResultsActivity
import com.iuvo.ui.activities.SalesActivity
import com.iuvo.ui.activities.ServicesActivity
import com.iuvo.utils.Store

/**
 * Created by apjoex on 25/02/2018.
 */

class ItemAdapter(val context : Context, val items : MutableList<ItemData>) : RecyclerView.Adapter<ItemAdapter.ViewHolder>() {

    var inflater = LayoutInflater.from(context)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = inflater.inflate(R.layout.item_view, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount() = items.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        val item = items[position]
        holder.label?.text = item.name
        holder.label?.setCompoundDrawablesWithIntrinsicBounds(0, item.drawable, 0, 0);

        holder.itemCard?.setOnClickListener {
            Store.item = item
            context.startActivity(Intent(context, ResultsActivity::class.java))

        }

    }

    class ViewHolder(itemView : View) : RecyclerView.ViewHolder(itemView) {
        var label = itemView.findViewById<TextView>(R.id.label)
        var itemCard = itemView.findViewById<CardView>(R.id.itemCard)
    }
}
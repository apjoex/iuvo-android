package com.iuvo.adapters

import android.content.Context
import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import com.iuvo.R
import com.iuvo.models.Result
import com.iuvo.ui.activities.ViewBusinessActivity
import com.iuvo.utils.Store
import org.jetbrains.anko.toast

class OwnerBusinessAdapter(private val mContext: Context, private val mResults: MutableList<Result>): RecyclerView.Adapter<OwnerBusinessAdapter.ViewHolder>() {

    private val layoutInflater = LayoutInflater.from(mContext)
    private var distance = ""

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = layoutInflater.inflate(R.layout.places_item_owner, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount() = mResults.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        val place = mResults[position]

        holder.title.text = place.name
        holder.vincinity.text = place.vicinity

        holder.thumbNail.setImageResource(R.drawable.ic_business_owner)

        holder.viewBtn.setOnClickListener {
            Store.currentlyViewedPlace = place
            mContext.startActivity(Intent(mContext, ViewBusinessActivity::class.java))
        }
    }


    class ViewHolder(itemView : View) : RecyclerView.ViewHolder(itemView) {
        val thumbNail : ImageView = itemView.findViewById(R.id.thumbNail)
        val title : TextView = itemView.findViewById(R.id.title)
        val vincinity : TextView = itemView.findViewById(R.id.vincinity)
        val viewBtn : Button = itemView.findViewById(R.id.viewBtn)
    }
}
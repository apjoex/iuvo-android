package com.iuvo.adapters

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.iuvo.R
import com.iuvo.models.ProviderData

/**
 * Created by apjoex on 03/03/2018.
 */
class FirebaseResultAdapter(context : Context, items : MutableList<ProviderData>) : RecyclerView.Adapter<FirebaseResultAdapter.ViewHolder>() {

    var mContext = context;
    var mItems = items;
    var inflater = LayoutInflater.from(context)


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = inflater.inflate(R.layout.result_item_view, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return mItems.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        val provider = mItems[position]
        holder.name?.setText(provider.name)
        holder.address?.setText(provider.address)
    }


    class ViewHolder(itemView : View) : RecyclerView.ViewHolder(itemView) {
        var name = itemView.findViewById<TextView>(R.id.name)
        var address = itemView.findViewById<TextView>(R.id.address)
    }
}
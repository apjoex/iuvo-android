package com.iuvo.adapters

import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.support.constraint.ConstraintLayout
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RatingBar
import android.widget.TextView
import com.google.android.gms.maps.model.LatLng
import com.iuvo.R
import com.iuvo.models.ItemData
import com.iuvo.models.Result
import com.iuvo.ui.activities.MapsActivity
import com.iuvo.utils.DistanceCalculator
import com.iuvo.utils.Store

class PlacesResultAdapter(private val mContext: Context, private val mResults: MutableList<Result>, private val mLatLng: LatLng?, private val selectedItem: ItemData): RecyclerView.Adapter<PlacesResultAdapter.ViewHolder>() {

    private val layoutInflater = LayoutInflater.from(mContext)
    private var distance = ""

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = layoutInflater.inflate(R.layout.places_item, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount() = mResults.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        val place = mResults[position]

        holder.title.text = place.name
        holder.vincinity.text = place.vicinity

        if (place.opening_hours?.open_now == true){
            holder.status.text = "OPEN"
            holder.status.setTextColor(Color.parseColor("#05DF29"))
        }else{
            holder.status.text = "CLOSED"
            holder.status.setTextColor(Color.parseColor("#FF0000"))
        }

        holder.ratingbar.rating = place.rating ?: 0F

        if (mLatLng != null){
            val kmDistance = DistanceCalculator().greatCircleInKilometers(mLatLng.latitude, mLatLng.longitude, place.geometry!!.location!!.lat, place.geometry!!.location!!.lng)
            distance = String.format("%.2f", kmDistance)
            holder.distance.text = "$distance Km away"
        }

        holder.thumbNail.setImageResource(selectedItem.drawable)

        holder.body.setOnClickListener {
            Store.currentlyViewedPlace = place
            Store.selectedItem = selectedItem
            Store.distance = distance
            val intent = Intent(mContext, MapsActivity::class.java)
            mContext.startActivity(intent)
        }
    }


    class ViewHolder(itemView : View) : RecyclerView.ViewHolder(itemView) {
        val body : ConstraintLayout = itemView.findViewById(R.id.body)
        val thumbNail : ImageView = itemView.findViewById(R.id.thumbNail)
        val ratingbar : RatingBar = itemView.findViewById(R.id.viewBtn)
        val title : TextView = itemView.findViewById(R.id.title)
        val status : TextView = itemView.findViewById(R.id.status)
        val vincinity : TextView = itemView.findViewById(R.id.vincinity)
        val distance : TextView = itemView.findViewById(R.id.distance)
    }
}
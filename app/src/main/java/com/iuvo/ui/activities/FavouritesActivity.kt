package com.iuvo.ui.activities

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.View
import android.view.WindowManager
import android.widget.Toast
import com.github.kittinunf.fuel.android.extension.responseJson
import com.github.kittinunf.fuel.httpGet
import com.google.android.gms.maps.model.LatLng
import com.google.firebase.database.*
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.iuvo.R
import com.iuvo.adapters.PlacesResultAdapter
import com.iuvo.models.ItemData
import com.iuvo.models.Result
import com.iuvo.models.UserData
import com.iuvo.utils.BaseUtil
import com.iuvo.utils.Store.selectedItem
import kotlinx.android.synthetic.main.activity_favourites.*
import org.json.JSONException
import org.json.JSONObject

class FavouritesActivity : AppCompatActivity() {

    private lateinit var loggedInUser : UserData
    private lateinit var favDbRef : DatabaseReference
    private var favPlaces = mutableListOf<Result>()
    private var keysCount = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN)
        setContentView(R.layout.activity_favourites)

        loggedInUser = BaseUtil(self@this).loggedInUser
        favDbRef = FirebaseDatabase.getInstance().reference.child("favourites").child(loggedInUser.key)

        toolbar.setMinMode(!loggedInUser.isGeneralUser)

        getUserFavourites()

    }

    private fun getUserFavourites() {

        favDbRef.addListenerForSingleValueEvent(object : ValueEventListener{
            override fun onCancelled(p0: DatabaseError?) {
                loader.visibility = View.GONE
                noFav.visibility = View.VISIBLE
            }

            override fun onDataChange(dataSnapshot: DataSnapshot?) {
                if (dataSnapshot?.exists() == true){
                    keysCount = dataSnapshot.childrenCount.toInt()

                    dataSnapshot.children.forEach {

                        if(it.value as Boolean){
                            getGooglePlace(placeID= it.key)
                        }else{
                            getCustomPlace(it.key)
                        }
                    }
                }else{
                    loader.visibility = View.GONE
                    noFav.visibility = View.VISIBLE
                }
            }
        })
    }

    private fun getCustomPlace(placeID: String?) {
        val goodPath = placeID.toString().replace("***","/",true)
        FirebaseDatabase.getInstance().reference.child("businesses").child(goodPath).addListenerForSingleValueEvent(object : ValueEventListener{
            override fun onCancelled(p0: DatabaseError?) {
            }

            override fun onDataChange(dataSnap: DataSnapshot?) {
                if (dataSnap?.exists() == true){
                    val place = dataSnap.getValue(Result::class.java)
                    Log.d("POSSSTT", place.toString())
                    if (place != null){
                        favPlaces.add(place)
                        if(favPlaces.size == keysCount) showFavPlaces()
                    }
                }
            }
        })
    }

    private fun getGooglePlace(placeID: String) {
        val placeURL = "https://maps.googleapis.com/maps/api/place/details/json?placeid=$placeID&key=${getString(R.string.API_KEY)}"
        placeURL.httpGet().responseJson { _, _, result ->
            result.fold({json ->
                try {
                    val baseObject = json.obj()
                    val resultObject = baseObject.getJSONObject("result")

                    val type = object  : TypeToken<Result>(){}.type
                    val place : Result = Gson().fromJson(resultObject.toString(), type)

                    favPlaces.add(place)
                    if(favPlaces.size == keysCount)  showFavPlaces()

                }catch (e : JSONException){
                    Toast.makeText(this@FavouritesActivity, e.localizedMessage, Toast.LENGTH_SHORT).show()
                }
            },{fuelError ->
                return@responseJson
            })
        }
    }

    private fun showFavPlaces() {
        loader.visibility = View.INVISIBLE

        val layoutManager = LinearLayoutManager(this@FavouritesActivity)
        list.layoutManager = layoutManager

        val favItem = ItemData("","", R.drawable.ic_star_white,true)
        val adapter = PlacesResultAdapter(this@FavouritesActivity, favPlaces, null, favItem)
        list.adapter = adapter
    }
}

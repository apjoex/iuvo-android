package com.iuvo.ui.activities

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.view.WindowManager
import com.iuvo.R
import com.iuvo.ui.activities.auth.LogIn
import com.iuvo.ui.activities.onboarding.OnBoardingActivity
import com.iuvo.utils.BaseUtil

class SplashScreen : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN)
        setContentView(R.layout.activity_splash_screen)

        Handler().postDelayed({
            val intent = if (BaseUtil(this).isLoggedIn){
                Intent(this, Home::class.java)
            }else{
                Intent(this, OnBoardingActivity::class.java)
            }
            startActivity(intent)
            finish()
        }, 2000)
    }
}

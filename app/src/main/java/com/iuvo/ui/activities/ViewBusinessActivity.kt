package com.iuvo.ui.activities

import android.app.Activity
import android.app.ProgressDialog
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.provider.MediaStore
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.view.View
import android.view.WindowManager
import android.widget.Toast
import com.firebase.geofire.GeoFire
import com.firebase.geofire.GeoLocation
import com.google.android.gms.common.GooglePlayServicesNotAvailableException
import com.google.android.gms.location.places.AutocompleteFilter
import com.google.android.gms.location.places.ui.PlaceAutocomplete
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.storage.FirebaseStorage
import com.iuvo.R
import com.iuvo.models.Photo
import com.iuvo.models.Result
import com.iuvo.models.UserData
import com.iuvo.utils.BaseUtil
import com.iuvo.utils.Store
import com.yalantis.ucrop.UCrop
import kotlinx.android.synthetic.main.activity_view_business.*
import org.jetbrains.anko.alert
import org.jetbrains.anko.okButton
import org.jetbrains.anko.selector
import org.jetbrains.anko.toast
import java.io.File

class ViewBusinessActivity : AppCompatActivity() {

    companion object {
        private const val ADDRESS_REQUEST = 10
        private const val REQUEST_CODE: Int = 100
        private const val PICS_FROM_CAMERA: Int = 101
        private const val PICS_FROM_GALLERY: Int = 102
    }

    private var destinationUri : Uri  = Uri.EMPTY
    private var uriName = ""

    private var newAddressSet = false
    private var newLat = 0.0
    private var newLng = 0.0

    private val place : Result
        get() = Store.currentlyViewedPlace
    private val loggedInUser : UserData
        get() = BaseUtil(this).loggedInUser


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN)
        setContentView(R.layout.activity_view_business)

        supportActionBar?.title = place.name

        enableAllFields(false)
        updateUI()

        clickEvents()
    }

    private fun clickEvents() {
        actionBtn.setOnClickListener {

            if (actionBtn.text == "Edit"){
                selector("Edit", listOf("Edit Business Details", "Add photo")) { dialogInterface, i ->
                    dialogInterface.dismiss()
                    if (i == 0){
                        enableAllFields(true)
                        actionBtn.text = "Save"
                    }else{
                        choosePhoto()
                    }
                }
            }else{
                saveChanges()
            }
        }

        addressInput.editText?.setOnClickListener {
            getAddressFromGooglePlaces()
        }
    }

    private fun getAddressFromGooglePlaces() {
        try {
            val intent = PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_OVERLAY)
                    .setFilter(AutocompleteFilter.Builder().setCountry("NG").build())
                    .build(this)
            startActivityForResult(intent, ADDRESS_REQUEST)
        }catch (e : GooglePlayServicesNotAvailableException){
            e.printStackTrace()
        }
    }

    private fun saveChanges() {

        val newProps = mutableMapOf(
                "name" to nameInput.editText?.text.toString(),
                "mail" to emailInput.editText?.text.toString(),
                "phone" to phoneInput.editText?.text.toString(),
                "website" to webInput.editText?.text.toString()
        )

        if (newAddressSet){
            //Update GeoFire
            newProps["vicinity"] = addressInput.editText?.text.toString()

            val geoRef = FirebaseDatabase.getInstance().reference.child("geofire").child(place.category)
            val geoFire = GeoFire(geoRef)
            geoFire.setLocation(place.place_id, GeoLocation(newLat, newLng))

        }

        FirebaseDatabase.getInstance().reference.child("businesses").child(place.category).child(place.place_id).updateChildren(newProps as Map<String, Any>?).addOnCompleteListener { task ->
            if (task.isSuccessful){
                alert("Business updated successfully."){
                    isCancelable = false
                    okButton { dialog ->
                        dialog.dismiss()
                        finish()
                    }
                }.show()
            }
        }
    }

    private fun choosePhoto() {
        selector("Add photo", listOf("Choose photo", "Take photo")){dialogInterface, i ->
            dialogInterface.dismiss()
            if (i == 0){
                chooseFromGallery()
            }else{
                chooseFromCamera()
            }
            actionBtn.text = "Save"
        }
    }

    private fun chooseFromGallery() {
        val intent = Intent()
        intent.type = "image/*"
        intent.action = Intent.ACTION_GET_CONTENT
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICS_FROM_GALLERY)
    }

    private fun chooseFromCamera() {
        if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED ||
                ContextCompat.checkSelfPermission(this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED ||
                ContextCompat.checkSelfPermission(this, android.Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            //Request for permissions
            ActivityCompat.requestPermissions(this, arrayOf(android.Manifest.permission.CAMERA, android.Manifest.permission.WRITE_EXTERNAL_STORAGE, android.Manifest.permission.READ_EXTERNAL_STORAGE), REQUEST_CODE)
        } else {
            //Permissions granted
            capturePicture()
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        if (requestCode == REQUEST_CODE) {
            if (grantResults.isNotEmpty() and (grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                capturePicture()
            } else {
                Toast.makeText(this, "Err. We kinda need those permissions to proceed", Toast.LENGTH_SHORT).show()
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        when (requestCode) {
            PICS_FROM_CAMERA -> if (resultCode == Activity.RESULT_OK) cropImage(mUri = data?.data)
            PICS_FROM_GALLERY -> if (resultCode == Activity.RESULT_OK) cropImage(mUri = data?.data)
            69 -> {
                if (resultCode == -1){
                    destinationUri = UCrop.getOutput(data!!)!!
                    uploadPhotoForBusiness(destinationUri)
                }
            }
            ADDRESS_REQUEST -> {
                when (resultCode) {
                    Activity.RESULT_OK -> {

                        newAddressSet = true

                        val place = PlaceAutocomplete.getPlace(this, data)

                        newLat = place.latLng.latitude
                        newLng = place.latLng.longitude

                        addressInput.editText?.setText(place.address)

                    }
                    PlaceAutocomplete.RESULT_ERROR -> {
                        val status = PlaceAutocomplete.getStatus(this, data)
                        toast(status.statusMessage ?: "Unable to get place")
                    }
                    Activity.RESULT_CANCELED -> toast("Action cancelled")
                }
            }
        }
    }

    private fun uploadPhotoForBusiness(destinationUri: Uri) {
        val pDialog = ProgressDialog.show(self@ this, null, "Uploading photo for business...", false, false)
        FirebaseStorage.getInstance().reference.child("avatar/$uriName.jpg").putFile(destinationUri).addOnCompleteListener { task ->
            pDialog.dismiss()
            if (task.isSuccessful){
                val downloadURL = task.result.downloadUrl.toString()

                val photo = Photo(downloadURL)
                FirebaseDatabase.getInstance().reference.child("photos").child(place.place_id).push().setValue(photo)
                        .addOnCompleteListener { newTask ->
                            if (newTask.isSuccessful){
                                alert("Business photo uploaded successfully."){
                                    isCancelable = false
                                    okButton { dialog ->
                                        dialog.dismiss()
                                        finish()
                                    }
                                }.show()
                            }
                        }

            }else{
                toast("Business photo was not uploaded. Please try again")
            }
        }
    }

    private fun cropImage(mUri: Uri?) {

        val options = UCrop.Options()
        options.setToolbarColor(resources.getColor(R.color.colorCard))
        options.setStatusBarColor(resources.getColor(R.color.colorCard))

        uriName = "${System.currentTimeMillis()}.jpg"
        UCrop.of(mUri!!, Uri.fromFile(File(cacheDir, uriName)))
                .withOptions(options)
                .withMaxResultSize(300, 300)
                .start(this);
    }

    private fun capturePicture() {
        val takePictureIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        if (takePictureIntent.resolveActivity(packageManager) != null) {
            startActivityForResult(takePictureIntent, PICS_FROM_CAMERA)
        }
    }

    private fun enableAllFields(status: Boolean) {
        nameInput.isEnabled = status
        phoneInput.isEnabled = status
        emailInput.isEnabled = status
        webInput.isEnabled = status
        addressInput.isEnabled = status
    }

    private fun updateUI() {
        nameInput.editText?.setText(place.name)
        phoneInput.editText?.setText(place.phone)
        emailInput.editText?.setText(place.mail)
        webInput.editText?.setText(place.website)
        addressInput.editText?.setText(place.vicinity)
    }
}

package com.iuvo.ui.activities

import android.app.Activity
import android.app.ProgressDialog
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Patterns
import android.view.View
import android.view.WindowManager
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.SpinnerAdapter
import com.firebase.geofire.GeoFire
import com.firebase.geofire.GeoLocation
import com.google.android.gms.common.GooglePlayServicesNotAvailableException
import com.google.android.gms.location.places.AutocompleteFilter
import com.google.android.gms.location.places.ui.PlaceAutocomplete
import com.google.firebase.database.FirebaseDatabase
import com.iuvo.R
import com.iuvo.models.*
import com.iuvo.utils.BaseUtil
import com.iuvo.utils.DataUtils
import kotlinx.android.synthetic.main.activity_add_business.*
import org.jetbrains.anko.toast

class AddBusinessActivity : AppCompatActivity() {

    companion object {
        const val ADDRESS_REQUEST = 10
    }
    private lateinit var businessLocation : Location
    private lateinit var businessGeometry : Geometry
    private lateinit var businessOpenHour : OpeningHour
    private lateinit var business : Result
    private var spinnerItems : MutableList<ItemData> = mutableListOf()
    private lateinit var pDialog : ProgressDialog
    private val loggedInUser : UserData
        get() = BaseUtil(this).loggedInUser

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN)
        setContentView(R.layout.activity_add_business)

        businessLocation = Location(0.0, 0.0)
        businessGeometry = Geometry(businessLocation)
        businessOpenHour = OpeningHour(true)
        business = Result(businessGeometry, "", "", "", businessOpenHour, null, "", 0F, "", "")

        clickEvents()
    }

    private fun clickEvents() {

        natureSpinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener{
            override fun onNothingSelected(p0: AdapterView<*>?) {

            }

            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                when (p2){
                    0 -> clearTypes()
                    1 -> getTypesForServices()
                    2 -> getTypesForRepairs()
                    3 -> getTypesForSales()
                }
            }
        }

        businessAddress.setOnClickListener {
           getAddressFromGooglePlaces()
        }

        addBtn.setOnClickListener {
            if (validateFields()){

                pDialog = ProgressDialog.show(this@AddBusinessActivity, "Please wait", "Uploading business details", false, false)

                val fbPath = BaseUtil(this).makeFirebasePathFriendly(spinnerItems[typeSpinner.selectedItemPosition].name)
                val newKey = FirebaseDatabase.getInstance().reference.child("businesses").child(fbPath).push().key

                business.id = newKey
                business.name = businessName.text.toString()
                business.place_id = newKey
                business.reference = newKey
                business.mail = businessEmail.text.toString()
                business.phone = businessPhone.text.toString()
                business.website = businessWeb.text.toString()
                business.customSource = true
                business.ownerID = loggedInUser.key
                business.ownerName = loggedInUser.fullNmae
                business.category = fbPath

                //Set up geofire
                val geoRef = FirebaseDatabase.getInstance().reference.child("geofire").child(fbPath)
                val geoFire = GeoFire(geoRef)
                geoFire.setLocation(newKey, GeoLocation(business.geometry!!.location!!.lat, business.geometry!!.location!!.lng))


                //Save to database
                FirebaseDatabase.getInstance().reference.child("businesses").child(fbPath).child(newKey).setValue(business).addOnCompleteListener { task ->
                    if (task.isSuccessful){
                        tieBusinessToOwner(fbPath, newKey)

                        //Register for forum
                        FirebaseDatabase.getInstance().reference.child("users").child(loggedInUser.key).child("forums").child(spinnerItems[typeSpinner.selectedItemPosition].name).setValue(true)
                    }else{
                        pDialog.dismiss()
                        toast("Unable to upload business details. Please try again")
                    }
                }

            }
        }
    }

    private fun tieBusinessToOwner(fbPath: String, newKey: String) {

        FirebaseDatabase.getInstance().reference.child("users").child(loggedInUser.key).child("businesses").child("$fbPath***$newKey").setValue(true).addOnCompleteListener { task ->
            pDialog.dismiss()
            if (task.isSuccessful){
                toast("Business details uploaded successfully")
                onBackPressed()
            }else{
                toast("Unable to link business.")
            }
        }
    }

    private fun validateFields(): Boolean {
        if (natureSpinner.selectedItemPosition == 0){
            toast("Please choose a nature of business")
            return false
        }

        if (businessName.text.isBlank()){
            toast("Please enter a business name")
            return false
        }

        if (businessPhone.text.length < 11 ){
            toast("Please enter a business phone number")
            return false
        }

        if (!Patterns.EMAIL_ADDRESS.matcher(businessEmail.text.toString()).matches()){
            toast("Please enter a business email address")
            return false
        }

        if (businessAddress.text.isBlank()){
            toast("Please enter a business address")
            return false
        }

        return true
    }

    private fun getAddressFromGooglePlaces() {
        try {
            val intent = PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_OVERLAY)
                    .setFilter(AutocompleteFilter.Builder().setCountry("NG").build())
                    .build(this)
            startActivityForResult(intent, ADDRESS_REQUEST)
        }catch (e : GooglePlayServicesNotAvailableException){
            e.printStackTrace()
        }
    }

    private fun clearTypes() {
        typeSpinner.adapter = null
    }

    private fun getTypesForServices() {
        spinnerItems.clear()
        spinnerItems = DataUtils.getServices().filterNot { it.isFromGoogle }.toMutableList()
        showList()
    }

    private fun getTypesForSales() {
        spinnerItems.clear()
        spinnerItems = DataUtils.getSales()
        showList()
    }

    private fun getTypesForRepairs() {
        spinnerItems.clear()
        spinnerItems = DataUtils.getRepairs()
        showList()
    }

    private fun showList() {
        val spinnerAdapter = ArrayAdapter(this@AddBusinessActivity, android.R.layout.simple_spinner_dropdown_item, spinnerItems) as SpinnerAdapter
        typeSpinner.adapter = spinnerAdapter
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == ADDRESS_REQUEST) {
            when (resultCode) {
                Activity.RESULT_OK -> {
                    val place = PlaceAutocomplete.getPlace(this, data)

                    businessLocation.lat = place.latLng.latitude
                    businessLocation.lng = place.latLng.longitude
                    businessGeometry.location = businessLocation

                    businessOpenHour.open_now = true

                    business.opening_hours = businessOpenHour
                    business.geometry = businessGeometry
                    business.vicinity = place.address.toString()

                    businessAddress.setText(business.vicinity)


                }
                PlaceAutocomplete.RESULT_ERROR -> {
                    val status = PlaceAutocomplete.getStatus(this, data)
                    toast(status.statusMessage ?: "Unable to get place")
                }
                Activity.RESULT_CANCELED -> toast("Action cancelled")
            }
        }
    }
}

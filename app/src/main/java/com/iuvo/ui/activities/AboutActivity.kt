package com.iuvo.ui.activities

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.WindowManager
import com.iuvo.BuildConfig
import com.iuvo.R
import com.iuvo.models.UserData
import com.iuvo.utils.BaseUtil
import kotlinx.android.synthetic.main.activity_about.*

class AboutActivity : AppCompatActivity() {

    private val loggedInUser : UserData
        get() = BaseUtil(this@AboutActivity).loggedInUser

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN)
        setContentView(R.layout.activity_about)

        toolbar.setMinMode(!loggedInUser.isGeneralUser)

        versionLabel.text = "Version ${BuildConfig.VERSION_NAME}"
    }
}

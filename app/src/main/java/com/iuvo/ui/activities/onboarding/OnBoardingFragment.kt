package com.iuvo.ui.activities.onboarding

import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import com.iuvo.R
import com.iuvo.ui.activities.auth.LogIn
import kotlinx.android.synthetic.main.onboarding_fragment.*

class OnBoardingFragment: Fragment() {
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.onboarding_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val dataImg = arguments?.getInt("drawable") ?: R.drawable.one
        val dataText = arguments?.getString("text")

        img.setImageResource(dataImg)
        text.text = dataText

        startBtn.showIf(dataText?.contains("Live chat", true) == true)
        startBtn.setOnClickListener {
            val intent = Intent(activity, LogIn::class.java)
            startActivity(intent)
            activity?.finish()
        }
    }

    private fun Button.showIf(condition: Boolean){
        if (condition){
            this.visibility = View.VISIBLE
        }else{
            this.visibility = View.GONE
        }
    }
}


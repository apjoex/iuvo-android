package com.iuvo.ui.activities

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import android.view.WindowManager
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import com.iuvo.R
import com.iuvo.adapters.ChannelListAdapter
import com.iuvo.models.UserData
import com.iuvo.utils.BaseUtil
import kotlinx.android.synthetic.main.activity_inbox.*

class InboxActivity : AppCompatActivity() {

    private val loggedInUser : UserData
        get() = BaseUtil(this).loggedInUser

    private var channels = mutableListOf<Pair<String, String>>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN)
        setContentView(R.layout.activity_inbox)

        toolbar.setMinMode(!loggedInUser.isGeneralUser)

        getChannels()
    }

    private fun getChannels() {
        val channelsRef = FirebaseDatabase.getInstance().reference.child("users").child(loggedInUser.key).child("chatChannels")

        channelsRef.addValueEventListener(object : ValueEventListener{
            override fun onCancelled(p0: DatabaseError?) {
                placeHolderText.text = "Unable to retrieve direct messages"
            }

            override fun onDataChange(snapShot: DataSnapshot?) {
                if (snapShot?.exists() == true){
                    snapShot.children.forEach {
                        channels.add(it.key to it.value as String)
                    }

                    showChannels()
                }else{
                    placeHolderText.text = "You have no direct messages"
                }
            }
        })
    }

    private fun showChannels() {
        if (channels.isNotEmpty()){

            val layoutManager = LinearLayoutManager(this)
            channelsList.layoutManager = layoutManager

            val adapter = ChannelListAdapter(this, channels)
            channelsList.adapter = adapter

            placeHolder.visibility = View.GONE
        }
    }
}

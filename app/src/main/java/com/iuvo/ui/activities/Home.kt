package com.iuvo.ui.activities

import android.app.Activity
import android.app.NotificationManager
import android.app.ProgressDialog
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.support.design.widget.NavigationView
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.support.v4.view.GravityCompat
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.text.SpannableString
import android.text.style.ForegroundColorSpan
import android.util.Log
import android.view.Gravity
import android.view.View
import android.view.WindowManager
import android.widget.TextView
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import com.google.firebase.iid.FirebaseInstanceId
import com.google.firebase.storage.FirebaseStorage
import com.iuvo.R
import com.iuvo.adapters.OwnerBusinessAdapter
import com.iuvo.adapters.PlacesResultAdapter
import com.iuvo.models.ItemData
import com.iuvo.models.Result
import com.iuvo.models.UserData
import com.iuvo.utils.BaseUtil
import kotlinx.android.synthetic.main.activity_home.*
import kotlinx.android.synthetic.main.content_home.*
import kotlinx.android.synthetic.main.layout_header.*
import org.jetbrains.anko.*

class Home : AppCompatActivity() {

    companion object {
        private const val REQUEST_CODE: Int = 100
        private const val PICS_FROM_CAMERA: Int = 101
        private const val PICS_FROM_GALLERY: Int = 102
    }

    private var numberOfBusinesses = 0
    private var ownersBusinesses = mutableListOf<Result>()
    private lateinit var pDialog: ProgressDialog
    private lateinit var loggedInUser : UserData

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN)
        setContentView(R.layout.activity_home)

        setupLogOutMenu()
        loggedInUser = BaseUtil(this@Home).loggedInUser
        nav_view.getHeaderView(0).findViewById<TextView>(R.id.profileName).text = loggedInUser.fullNmae

        if (loggedInUser.isGeneralUser){
            //General user logged in
            logoBackground.visibility = View.VISIBLE
            searchContainer.visibility = View.VISIBLE
            nav_view.menu.findItem(R.id.nav_forum).isVisible = false
        }else{
            //Business owner logged in
            logoBackground.visibility = View.GONE
            searchContainer.visibility = View.GONE

            nav_view.menu.findItem(R.id.nav_fav).isVisible = false
            nav_view.menu.findItem(R.id.nav_settings).isVisible = false
            servicesCard.visibility = View.GONE
            salesCard.visibility = View.GONE
            repairsCard.visibility = View.GONE

        }

        updateFCMToken()

        val notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        notificationManager.cancelAll()

        clickEvents()

        nav_view.setNavigationItemSelectedListener(listener)
    }

    private fun updateFCMToken() {
        FirebaseDatabase.getInstance().reference.child("users").child(loggedInUser.key).child("fcmToken").setValue(FirebaseInstanceId.getInstance().token)
    }

    private fun getMyBusiness() {
        FirebaseDatabase.getInstance().reference.child("users").child(loggedInUser.key).child("businesses").addValueEventListener(object : ValueEventListener{
            override fun onCancelled(p0: DatabaseError?) {
            }

            override fun onDataChange(dataSnap: DataSnapshot?) {
                if (dataSnap?.exists() == true){
                    ownersBusinesses.clear()
                    numberOfBusinesses = dataSnap.childrenCount.toInt()
                    dataSnap.children.forEach {
                        getBusinessData(placeID= it.key)
                    }
                }
            }
        })
    }

    private fun getBusinessData(placeID: String?) {
        val goodPath = placeID.toString().replace("***","/",true)
        FirebaseDatabase.getInstance().reference.child("businesses").child(goodPath).addValueEventListener(object : ValueEventListener{
            override fun onCancelled(p0: DatabaseError?) {
            }

            override fun onDataChange(dataSnap: DataSnapshot?) {
                if (dataSnap?.exists() == true){
                    val place = dataSnap.getValue(Result::class.java)
                    if (place != null){
                        ownersBusinesses.add(place)

                        if(ownersBusinesses.size == numberOfBusinesses){
                            showOwnerBusinesses()
                        }
                    }
                }
            }
        })
    }

    private fun showOwnerBusinesses() {
        businessPlaceHolder.visibility = View.GONE

        val layoutManager = LinearLayoutManager(this@Home)
        businessList.layoutManager = layoutManager

        val adapter = OwnerBusinessAdapter(this@Home, ownersBusinesses)
        businessList.adapter = adapter
    }

    private fun setupLogOutMenu() {
        val logOutMenuItem = nav_view.menu.findItem(R.id.nav_log_out)
        val spannableString = SpannableString(logOutMenuItem.title.toString())
        spannableString.setSpan(ForegroundColorSpan(ContextCompat.getColor(this, R.color.colorClosed)), 0, spannableString.length, 0)
        logOutMenuItem.title = spannableString
    }

    private fun clickEvents() {
        drawer_menu.setOnClickListener {
            drawer_layout.openDrawer(Gravity.START)
        }

        servicesCard.setOnClickListener {
            startActivity(Intent(this@Home, ServicesActivity::class.java))
        }

        salesCard.setOnClickListener {
            startActivity(Intent(this@Home, SalesActivity::class.java))
        }

        repairsCard.setOnClickListener {
            startActivity(Intent(this@Home, RepairsActivity::class.java))
        }

        uploadIdBtn.setOnClickListener {
            selector("Upload ID", listOf("Choose photo","Take photo")) { dialogInterface, i ->
                dialogInterface.dismiss()
                if (i == 0){
                    chooseFromGallery()
                }else{
                    chooseFromCamera()
                }
            }
        }

        addBusinessBtn.setOnClickListener {
            startActivity(Intent(this@Home, AddBusinessActivity::class.java))
        }

        searchContainer.setOnClickListener {
            val intent = Intent(this@Home, SearchActivity::class.java)
            startActivity(intent)
        }
    }

    private val listener = NavigationView.OnNavigationItemSelectedListener { item ->
        when(item.itemId){
            R.id.nav_forum -> startActivity(Intent(this@Home, ForumActivity::class.java))
            R.id.nav_inbox -> startActivity(Intent(this@Home, InboxActivity::class.java))
            R.id.nav_about -> startActivity(Intent(this@Home, AboutActivity::class.java))
            R.id.nav_settings -> startActivity(Intent(this@Home, SettingsActivity::class.java))
            R.id.nav_profile -> startActivity(Intent(this@Home, ProfileActivity::class.java))
            R.id.nav_fav -> startActivity(Intent(this@Home, FavouritesActivity::class.java))
            R.id.nav_log_out -> promptLogOut()
        }

        drawer_layout.closeDrawer(Gravity.START)
        true
    }

    private fun promptLogOut() {
        alert("Are you sure you want to log out?", "Log out"){
            yesButton { dialog ->
                dialog.dismiss()
                logOut()
            }
            noButton { dialog ->
                dialog.dismiss()
            }
        }.show()
    }

    private fun logOut() {
        FirebaseAuth.getInstance().signOut()
        BaseUtil(this@Home).preference.edit().clear().apply()
        val intent = Intent(this@Home, SplashScreen::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK
        startActivity(intent)
        finish()
    }

    override fun onBackPressed() {
        if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
            drawer_layout.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }

    override fun onResume() {
        super.onResume()
        nav_view.menu.getItem(0).isChecked = true
        loggedInUser = BaseUtil(this@Home).loggedInUser
        nav_view.getHeaderView(0).findViewById<TextView>(R.id.profileName).text = loggedInUser.fullNmae

        if (!loggedInUser.isGeneralUser){
            //FOR BUSINESS OWNERS
            if (loggedInUser.verified){
                verificationPending.visibility = View.GONE
                dashBoard.visibility = View.VISIBLE
                getMyBusiness()
            }else{
                nav_view.menu.findItem(R.id.nav_forum).isVisible = false
                nav_view.menu.findItem(R.id.nav_inbox).isVisible = false
                nav_view.menu.findItem(R.id.nav_profile).isVisible = false

                verificationPending.visibility = View.VISIBLE
                infoLabel.text = loggedInUser.declineMessage
                if (loggedInUser.needsUpload){
                    uploadIdBtn.visibility = View.VISIBLE
                }else{
                    uploadIdBtn.visibility = View.GONE
                }
            }
        }



    }

    private fun chooseFromGallery() {
        val intent = Intent()
        intent.type = "image/*"
        intent.action = Intent.ACTION_GET_CONTENT
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICS_FROM_GALLERY)
    }

    private fun chooseFromCamera() {
        if (ContextCompat.checkSelfPermission(self@ this, android.Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED ||
                ContextCompat.checkSelfPermission(self@ this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED ||
                ContextCompat.checkSelfPermission(self@ this, android.Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            //Request for permissions
            ActivityCompat.requestPermissions(self@ this, arrayOf(android.Manifest.permission.CAMERA, android.Manifest.permission.WRITE_EXTERNAL_STORAGE, android.Manifest.permission.READ_EXTERNAL_STORAGE), REQUEST_CODE)
        } else {
            //Permissions granted
            capturePicture()
        }
    }

    private fun capturePicture() {
        val takePictureIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        if (takePictureIntent.resolveActivity(packageManager) != null) {
            startActivityForResult(takePictureIntent, PICS_FROM_CAMERA)
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        if (requestCode == REQUEST_CODE) {
            if (grantResults.isNotEmpty() and (grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                capturePicture()
            } else {
                Toast.makeText(self@ this, "Err. We kinda need those permissions to proceed", Toast.LENGTH_SHORT).show()
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        when (requestCode) {
            PICS_FROM_CAMERA -> if (resultCode == Activity.RESULT_OK) uploadImageForVerification(mUri = data?.data)
            PICS_FROM_GALLERY -> if (resultCode == Activity.RESULT_OK) uploadImageForVerification(mUri = data?.data)
        }
    }

    private fun uploadImageForVerification(mUri: Uri?) {
        if (mUri != null){
            pDialog = ProgressDialog.show(self@ this, null, "Uploading image for verification...", false, false)
            FirebaseStorage.getInstance().reference.child("verification_images/${loggedInUser.key}.jpg").putFile(mUri).addOnCompleteListener { task ->
                pDialog.dismiss()
                if (task.isSuccessful){
                    val newVerificationRequestKey = FirebaseDatabase.getInstance().reference.child("verification_requests").push().key

                    FirebaseDatabase.getInstance().reference.child("users").child(loggedInUser.key).child("needsUpload").setValue(false)
                    FirebaseDatabase.getInstance().reference.child("users").child(loggedInUser.key).child("declineMessage").setValue("Your ID is awaiting verification...")
                    FirebaseDatabase.getInstance().reference.child("verification_requests").child(newVerificationRequestKey).child("userName").setValue(loggedInUser.fullNmae)
                    FirebaseDatabase.getInstance().reference.child("verification_requests").child(newVerificationRequestKey).child("userID").setValue(loggedInUser.key)
                    FirebaseDatabase.getInstance().reference.child("verification_requests").child(newVerificationRequestKey).child("imageURL").setValue(task.result.downloadUrl.toString()).addOnCompleteListener { task ->
                        if (task.isSuccessful){
                            alert("Your ID image has been uploaded and will be processed shortly", "Upload successful"){
                                isCancelable = false
                                okButton { dialog ->
                                    dialog.dismiss()
                                    logOut()
                                }
                            }.show()
                        }
                    }


                }else{
                    Toast.makeText(self@this, "Image upload was NOT successful", Toast.LENGTH_SHORT).show()
                }
            }
        }
    }


}

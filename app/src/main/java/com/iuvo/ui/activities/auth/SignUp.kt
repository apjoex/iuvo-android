package com.iuvo.ui.activities.auth

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.WindowManager
import com.iuvo.R
import kotlinx.android.synthetic.main.activity_sign_up.*

class SignUp : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN)
        setContentView(R.layout.activity_sign_up)

        supportActionBar?.elevation = 0F

        clickEvents()

    }

    private fun clickEvents() {

        boCard.setOnClickListener {
            startActivity(Intent(self@ this, BusinessOwnerSignUp::class.java))
        }

        guCard.setOnClickListener {
            startActivity(Intent(self@ this, GeneralUserSignUp::class.java))
        }
    }
}

package com.iuvo.ui.activities

import android.content.Context
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.GridLayoutManager
import android.view.WindowManager
import com.iuvo.R
import com.iuvo.adapters.ItemAdapter
import com.iuvo.utils.DataUtils
import kotlinx.android.synthetic.main.activity_sales.*

class SalesActivity : AppCompatActivity() {

    lateinit var context : Context

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN)
        setContentView(R.layout.activity_sales)

        context = this

        clickEvents()


        val layoutManager = GridLayoutManager(context,3)
        list.layoutManager = layoutManager
        val adapter = ItemAdapter(context, DataUtils.getSales())
        list.adapter = adapter
    }

    private fun clickEvents() {
        backBtn.setOnClickListener {
            onBackPressed()
        }

        searchContainer.setOnClickListener {
            val intent = Intent(this, SearchActivity::class.java)
            startActivity(intent)
        }
    }

}

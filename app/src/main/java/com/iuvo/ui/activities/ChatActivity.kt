package com.iuvo.ui.activities

import android.content.Context
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.CardView
import android.text.format.DateFormat
import android.view.Gravity
import android.view.View
import android.view.WindowManager
import android.view.inputmethod.InputMethodManager
import android.widget.PopupMenu
import android.widget.RelativeLayout
import android.widget.TextView
import com.firebase.ui.database.FirebaseListAdapter
import com.google.firebase.database.*
import com.iuvo.R
import com.iuvo.models.ChatMessage
import com.iuvo.models.NotifData
import com.iuvo.models.UserData
import com.iuvo.utils.BaseUtil
import kotlinx.android.synthetic.main.activity_add_business.*
import kotlinx.android.synthetic.main.activity_chat.*
import org.jetbrains.anko.email
import org.jetbrains.anko.toast


class ChatActivity : AppCompatActivity() {

    private lateinit var chatAdapter : FirebaseListAdapter<ChatMessage>
    private val loggedInUser : UserData
        get() = BaseUtil(this).loggedInUser

    private lateinit var name : String
    private lateinit var channelID : String
    private var isForum : Boolean = false
    private lateinit var chatRef : DatabaseReference

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN)
        setContentView(R.layout.activity_chat)

        window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);

        name = intent.getStringExtra("name")
        channelID = intent.getStringExtra("channelID")
        isForum = intent.getBooleanExtra("isForum", false)

        if (isForum){
            val forumChannelID = BaseUtil(this).makeFirebasePathFriendly(name)
            chatRef = FirebaseDatabase.getInstance().reference.child("forumChannels").child(forumChannelID).child("messages")
        }else{
            chatRef = FirebaseDatabase.getInstance().reference.child("chatChannels").child(channelID).child("messages")
        }

        toolbar.setToolbarTitle(name)
        toolbar.setMinMode(!loggedInUser.isGeneralUser)

        loadMessages()

        clickEvents()
    }

    private fun clickEvents() {
        sendBtn.setOnClickListener {
            if (newMsg.text.isNotBlank()){
                sendChatMessage(newMsg.text.toString())
            }else{
                toast("Enter chat message")
            }
        }

        moreBtn.setOnClickListener {
            val popUpMenu = PopupMenu(this@ChatActivity, moreBtn)
            popUpMenu.inflate(R.menu.report_menu)
            popUpMenu.setOnMenuItemClickListener {
                if (it.itemId == R.id.action_report){
                    //toast("Report user")
                    email("complaints@iuvohub.com","IUVO: Abuse Report","\n\nfrom ${loggedInUser.fullNmae}")
                }
                return@setOnMenuItemClickListener true
            }
            popUpMenu.show()
        }
    }

    private fun sendChatMessage(chatMessage: String) {
        val newChat = ChatMessage(chatMessage, loggedInUser.key, loggedInUser.fullNmae)
        chatRef.push().setValue(newChat)
        newMsg.setText("")
        if (newMsg != null) {
            val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(newMsg.windowToken, 0)
        }

        if (!isForum){

            //Send notifications for private messages

            val participants = channelID.split(", ")
            val otherUser = participants.first { it != loggedInUser.key }

            FirebaseDatabase.getInstance().reference.child("users").child(otherUser).child("fcmToken").addValueEventListener(object : ValueEventListener{
                override fun onCancelled(p0: DatabaseError?) {

                }

                override fun onDataChange(data: DataSnapshot?) {
                    if (data?.exists() == true){
                        val otherUserFCM = data.value as String
                        sendForPushNotification(chatMessage, otherUserFCM, loggedInUser)
                    }

                }
            })
        }
    }

    private fun sendForPushNotification(chatMessage: String, otherUserFCM: String, loggedInUser: UserData) {
        val notifData = NotifData(chatMessage, otherUserFCM, loggedInUser.fullNmae)
        FirebaseDatabase.getInstance().reference.child("notificationsList").push().setValue(notifData)
    }

    private fun loadMessages() {

        //Get messages
        chatAdapter = object : FirebaseListAdapter<ChatMessage>(this, ChatMessage::class.java, R.layout.message_item, chatRef){
            override fun populateView(v: View?, model: ChatMessage?, position: Int) {

                val messageText = v!!.findViewById(R.id.message_text) as TextView
                val messageTime = v.findViewById(R.id.message_time) as TextView
                val body = v.findViewById(R.id.body) as RelativeLayout
                val textCard = v.findViewById(R.id.text_card) as CardView

                // Set their text
                messageText.text = model?.text ?: ""
                if (model?.senderID == loggedInUser.key) {
                    body.gravity = Gravity.END
                    messageText.setTextColor(resources.getColor(R.color.colorWhite))
                    textCard.setBackgroundColor(resources.getColor(R.color.colorCard))
                    messageTime.gravity = Gravity.END
                } else {
                    body.gravity = Gravity.START
                    messageText.setTextColor(resources.getColor(R.color.colorAccent))
                    textCard.setBackgroundColor(resources.getColor(R.color.colorWhite))
                    messageTime.gravity = Gravity.START
                }

                messageTime.text = DateFormat.format("dd/MM/yy HH:mm", model!!.time)
                if (isForum){
                    messageTime.text = "${model.senderName}, ${DateFormat.format("dd/MM/yy HH:mm", model!!.time)}"
                }else{
                    //"yyyy-MM-dd hh:mm:ss a
                    messageTime.text = DateFormat.format("dd/MM/yy HH:mm", model.time)
                }

            }
        }

        chatListView.adapter = chatAdapter
        chatListView.setSelection(chatAdapter.count - 1)
    }

    override fun onResume() {
        super.onResume()
        chatListView.setSelection(chatAdapter.count - 1)
    }
}

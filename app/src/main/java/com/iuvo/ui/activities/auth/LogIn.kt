@file:Suppress("DEPRECATION")

package com.iuvo.ui.activities.auth

import android.app.ProgressDialog
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Patterns
import android.view.WindowManager
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import com.iuvo.R
import com.iuvo.models.UserData
import com.iuvo.ui.activities.ForgotPassword
import com.iuvo.ui.activities.Home
import com.iuvo.utils.BaseUtil
import kotlinx.android.synthetic.main.activity_log_in.*
import org.jetbrains.anko.alert

class LogIn : AppCompatActivity() {

    companion object {
        private const val MINIMUM_CHARACTER_LENGTH = 8
    }

    private lateinit var pDialog: ProgressDialog

    private val mAuth : FirebaseAuth?
        get() = FirebaseAuth.getInstance()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN)
        setContentView(R.layout.activity_log_in)

        clickEvents()
    }

    private fun clickEvents() {
        signIn_Btn.setOnClickListener {
           if (validateFields().first){
               logInUser(email = email.text.toString(), password= password.text.toString())
           }else{
               Toast.makeText(self@this, validateFields().second, Toast.LENGTH_SHORT).show()
           }
        }

        forgot_Btn.setOnClickListener {
            startActivity(Intent(self@this, ForgotPassword::class.java))
        }

        signUp_Btn.setOnClickListener {
            startActivity(Intent(self@this, SignUp::class.java))
        }
    }

    private fun logInUser(email: String, password: String) {
        pDialog = ProgressDialog.show(self@this, null, "Signing in...", false, false)
        mAuth?.signInWithEmailAndPassword(email, password)?.addOnCompleteListener { task ->
            if (task.isSuccessful){

                if (task.result.user.isEmailVerified || task.result.user.uid == "0VevZk1hGIYQVKlPsgW70QUNsrw2"){
                    val mFirUserUID = task.result.user.uid
                    getUserData(withId= mFirUserUID)
                }else{
                    pDialog.dismiss()
                    alert("Please click the link in the mail sent to ${task.result.user.email} to verify your email address", "Verify Email address"){
                        isCancelable = false
                        positiveButton("OKAY") {
                            it.dismiss()
                        }
                        negativeButton("RESEND LINK"){
                            it.dismiss()
                            task.result.user.sendEmailVerification()
                        }
                    }.show()
                }


            }else{
                pDialog.dismiss()
                Toast.makeText(self@this, "Unable to log in. Please check credentials and your internet connection", Toast.LENGTH_SHORT).show()
            }
        }
    }

    private fun getUserData(withId: String) {
        FirebaseDatabase.getInstance().reference.child("users").child(withId).addListenerForSingleValueEvent(object : ValueEventListener{
            override fun onCancelled(p0: DatabaseError?) {

            }

            override fun onDataChange(dataSnapshot: DataSnapshot?) {
                if (dataSnapshot != null){
                    val loggedInUser = dataSnapshot.getValue(UserData::class.java)

                    if (loggedInUser != null){
                        BaseUtil(this@LogIn).isLoggedIn = true
                        BaseUtil(this@LogIn).loggedInUser = loggedInUser
                    }

                    val intent = Intent(this@LogIn, Home::class.java)
                    intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK
                    startActivity(intent)
                    finish()
                }
            }
        })
    }

    private fun validateFields(): Pair<Boolean, String> {

        if (!Patterns.EMAIL_ADDRESS.matcher(email.text.toString()).matches()) return false to "Please enter a valid email address"
        if (password.text.isBlank() or (password.text.length < MINIMUM_CHARACTER_LENGTH)) return false to "Password must have a minimum of $MINIMUM_CHARACTER_LENGTH characters"

        return true to ""
    }
}

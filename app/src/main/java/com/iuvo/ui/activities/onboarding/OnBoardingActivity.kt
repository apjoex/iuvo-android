package com.iuvo.ui.activities.onboarding

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.WindowManager
import com.iuvo.R
import kotlinx.android.synthetic.main.activity_onboarding.*

class OnBoardingActivity : AppCompatActivity() {

    val data = listOf(OnBoardingData("Discover local business", R.drawable.one),
            OnBoardingData("Manage multiple Business listings", R.drawable.two),
            OnBoardingData("Live chat for your business", R.drawable.three))

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN)
        setContentView(R.layout.activity_onboarding)

        val adapter = OnBoardingAdapter(supportFragmentManager, data)
        pager.adapter = adapter
        worm_dots_indicator.setViewPager(pager)
    }
}

package com.iuvo.ui.activities

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Color
import android.os.Bundle
import android.support.constraint.ConstraintLayout
import android.support.design.widget.BottomSheetBehavior
import android.support.v4.app.ActivityCompat
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.View
import android.view.WindowManager
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.ceylonlabs.imageviewpopup.ImagePopup
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.google.firebase.database.*
import com.iuvo.R
import com.iuvo.models.Photo
import com.iuvo.models.Result
import com.iuvo.models.UserData
import com.iuvo.utils.BaseUtil
import com.iuvo.utils.Store
import kotlinx.android.synthetic.main.activity_maps.*
import kotlinx.android.synthetic.main.bottomsheet_info.*
import org.jetbrains.anko.*

class MapsActivity : AppCompatActivity(), OnMapReadyCallback {

    private lateinit var mMap: GoogleMap
    private lateinit var place : Result
    private lateinit var sheetBehavior: BottomSheetBehavior<ConstraintLayout>
    private lateinit var loggedInUser : UserData
    private lateinit var favDbRef : DatabaseReference

    companion object {
        private const val REQUEST_CODE = 10
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN)
        setContentView(R.layout.activity_maps)

        loggedInUser = BaseUtil(self@this).loggedInUser
        favDbRef = FirebaseDatabase.getInstance().reference.child("favourites").child(loggedInUser.key)


        place = Store.currentlyViewedPlace

        showPlaceDetails()

        toolbar.setToolbarTitle(place.name)
        sheetBehavior = BottomSheetBehavior.from(body)
        sheetBehavior.setBottomSheetCallback(object : BottomSheetBehavior.BottomSheetCallback() {
            override fun onSlide(bottomSheet: View, slideOffset: Float) {
            }
            override fun onStateChanged(bottomSheet: View, newState: Int) {
                Log.e("NEWSTATE", "$newState")
            }
        })

        clickEvents()

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        val mapFragment = supportFragmentManager.findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)

        checkForFavStatus()
    }

    private fun checkForFavStatus() {
        val path = if (place.customSource){
            "${place.category}***${place.place_id}"
        }else{
            place.place_id
        }
        favDbRef.child(path).addListenerForSingleValueEvent(object : ValueEventListener{
            override fun onCancelled(p0: DatabaseError?) {

            }

            override fun onDataChange(dataSnapshot: DataSnapshot?) {
                if (dataSnapshot?.exists() == true){
                    favBtn.text = "Remove from favourites"
                }else{
                    favBtn.text = "Add to favourites"
                }
            }
        })
    }

    private fun clickEvents() {

        body.setOnClickListener {
            sheetBehavior.state = BottomSheetBehavior.STATE_EXPANDED
        }

        favBtn.setOnClickListener {
            if (favBtn.text.contains("Add")){
                addToFavourites(place)
            }else if(favBtn.text.contains("Remove")){
                removeFromFavourites(place)
            }

        }

        chatBtn.setOnClickListener {
            alert("Do you want to start chatting with the business owner?","Chat"){
                positiveButton("YES") { dialog ->
                    dialog.dismiss()
                    //toast("Start chatting")
                    val membersID = mutableListOf(place.ownerID, loggedInUser.key)
                    membersID.sort()
                    startChat(generalUser=loggedInUser.key, businessOwner= place.ownerID, membersID = membersID)
                }
                negativeButton("NO") {dialog ->
                    dialog.dismiss()
                }
            }.show()
        }

        phoneBtn.setOnClickListener {
            alert("Do you want to call the business owner?","Call"){
                positiveButton("YES") { dialog ->
                    dialog.dismiss()
                    if(ActivityCompat.checkSelfPermission(this@MapsActivity, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED){
                        ActivityCompat.requestPermissions(this@MapsActivity, arrayOf(Manifest.permission.CALL_PHONE), REQUEST_CODE )
                    }else{
                        makeCall(place.phone)
                    }
                }
                negativeButton("NO") {dialog ->
                    dialog.dismiss()
                }
            }.show()
        }

        mailBtn.setOnClickListener {
            alert("Do you want to mail the business owner?","Send mail"){
                positiveButton("YES") { dialog ->
                    dialog.dismiss()
                    email(place.mail,"IUVO: Mail from Customer")
                }
                negativeButton("NO") {dialog ->
                    dialog.dismiss()
                }
            }.show()
        }

        webBtn.setOnClickListener {
            alert("Do you want to visit the business website?","Visit website"){
                positiveButton("YES") { dialog ->
                    dialog.dismiss()
                    browse("http://${place.website}")
                }
                negativeButton("NO") {dialog ->
                    dialog.dismiss()
                }
            }.show()
        }

    }

    private fun startChat(membersID: MutableList<String>, generalUser: String, businessOwner: String) {

        val chatChannelID = membersID.joinToString()

        //attach chat to business owner
        FirebaseDatabase.getInstance().reference.child("users").child(businessOwner).child("chatChannels").child(chatChannelID).setValue(loggedInUser.fullNmae)

        //attach chat to general user
        FirebaseDatabase.getInstance().reference.child("users").child(generalUser).child("chatChannels").child(chatChannelID).setValue(place.ownerName)

        val intent = Intent(this, ChatActivity::class.java)
        intent.putExtra("channelID", chatChannelID)
        intent.putExtra("name", place.ownerName)
        startActivity(intent)

    }

    private fun removeFromFavourites(place: Result) {

        val path = if (place.customSource){
            "${place.category}***${place.place_id}"
        }else{
            place.place_id
        }

        favDbRef.child(path).setValue(null).addOnCompleteListener { task ->
            if (task.isSuccessful){
                favBtn.text = "Add to favourites"
            }
        }


    }

    private fun addToFavourites(place: Result) {

        val path = if (place.customSource){
            "${place.category}***${place.place_id}"
        }else{
            place.place_id
        }

        favDbRef.child(path).setValue(true).addOnCompleteListener { task ->
            if (task.isSuccessful){
                favBtn.text = "Remove from favourites"
            }
        }
    }

    private fun showPlaceDetails() {

        placeTitle.text = place.name
        vincinity.text = place.vicinity
        viewBtn.rating = place.rating ?: 0F
        distance.text = "${Store.distance} Km away"
        thumbNail.setImageResource(Store.selectedItem.drawable)

        if (place.customSource){
            actionsBox.visibility = View.VISIBLE

            getPhotos()

            if(!place.website.isBlank()){
                webBtn.visibility = View.VISIBLE
            }

            if (!place.phone.isBlank()){
                phoneBtn.visibility = View.VISIBLE
            }

            if (!place.mail.isBlank()){
                mailBtn.visibility = View.VISIBLE
            }
        }else{
            actionsBox.visibility = View.GONE

            if(place.photos?.isNotEmpty() == true){
                //photos exist
                photosHolder.visibility = View.VISIBLE
                photosBox.removeAllViews()
                place.photos!!.forEach {
                    val photoView = layoutInflater.inflate(R.layout.photo_item, null, false)
                    val photoImg : ImageView = photoView.findViewById(R.id.img)
                    val photoUrl = getPhotoURL(it)
                    Log.d("urllll", photoUrl)
                    Glide.with(this@MapsActivity).load(photoUrl).into(photoImg)

                    val imgPopUp = ImagePopup(this@MapsActivity)
                    imgPopUp.isFullScreen = true
                    imgPopUp.isHideCloseIcon = true
                    imgPopUp.isImageOnClickClose = true
                    imgPopUp.backgroundColor = Color.BLACK
                    imgPopUp.initiatePopupWithGlide(photoUrl)

                    photoView.setOnClickListener {
                        imgPopUp.viewPopup()
                    }


                    photosBox.addView(photoView)
                }

                photosBox.invalidate()
            }
        }

    }

    private fun getPhotos() {

        FirebaseDatabase.getInstance().reference.child("photos").child(place.place_id).addListenerForSingleValueEvent(object : ValueEventListener{
            override fun onCancelled(p0: DatabaseError?) {

            }

            override fun onDataChange(dataSnapshot: DataSnapshot?) {
                if (dataSnapshot?.exists() == true){

                    photosHolder.visibility = View.VISIBLE
                    photosBox.removeAllViews()

                    dataSnapshot.children.forEach {

                        val photoView = layoutInflater.inflate(R.layout.photo_item, null, false)
                        val photoImg : ImageView = photoView.findViewById(R.id.img)

                        val mPhoto = it.getValue(Photo::class.java)

                        Glide.with(this@MapsActivity).load(mPhoto!!.photo_reference).into(photoImg)

                        val imgPopUp = ImagePopup(this@MapsActivity)
                        imgPopUp.isFullScreen = true
                        imgPopUp.isHideCloseIcon = true
                        imgPopUp.isImageOnClickClose = true
                        imgPopUp.backgroundColor = Color.BLACK
                        imgPopUp.initiatePopupWithGlide(mPhoto!!.photo_reference)

                        photoView.setOnClickListener {
                            imgPopUp.viewPopup()
                        }


                        photosBox.addView(photoView)
                    }

                    photosBox.invalidate()
                }
            }
        })
    }

    private fun getPhotoURL(it: Photo): String {
        val stringBuilder = StringBuilder("https://maps.googleapis.com/maps/api/place/photo?photoreference=${it.photo_reference}")
        stringBuilder.append("&sensor=false")
        stringBuilder.append("&maxheight=300")
        stringBuilder.append("&maxwidth=300")
        stringBuilder.append("&key=${getString(R.string.API_KEY)}")


        return stringBuilder.toString()
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap

        // Add a marker in Sydney and move the camera
        val coOrds = LatLng(place.geometry!!.location!!.lat, place.geometry!!.location!!.lng)
        mMap.addMarker(MarkerOptions().position(coOrds).title(place.name))
        //mMap.moveCamera(CameraUpdateFactory.newLatLng(co_ords))
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(coOrds, 18.0f))
        mMap.setPadding(0, 0, 0, BaseUtil(self@this).dpToPx(24))

        mMap.setOnMapClickListener {
            sheetBehavior.state = BottomSheetBehavior.STATE_COLLAPSED
        }

        mMap.setOnMarkerClickListener {
            sheetBehavior.state = BottomSheetBehavior.STATE_COLLAPSED
            return@setOnMarkerClickListener false
        }

    }
}

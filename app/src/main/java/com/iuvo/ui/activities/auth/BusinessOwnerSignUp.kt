@file:Suppress("DEPRECATION")

package com.iuvo.ui.activities.auth

import android.app.Activity
import android.app.ProgressDialog
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.text.Html
import android.text.method.LinkMovementMethod
import android.util.Patterns
import android.view.WindowManager
import android.widget.TextView
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.storage.FirebaseStorage
import com.iuvo.R
import com.iuvo.models.UserData
import com.iuvo.ui.activities.Home
import com.iuvo.utils.BaseUtil
import kotlinx.android.synthetic.main.activity_bosign_up.*

class BusinessOwnerSignUp : AppCompatActivity() {

    private companion object {
        private const val MINIMUM_CHARACTER_LENGTH = 8
        private const val REQUEST_CODE: Int = 100
        private const val PICS_FROM_CAMERA: Int = 101
        private const val PICS_FROM_GALLERY: Int = 102
    }

    private lateinit var userData : UserData
    private lateinit var pDialog: ProgressDialog

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN)
        setContentView(R.layout.activity_bosign_up)

        val policy = Html.fromHtml(getString(R.string.agree_terms_privacy))
        val termsOfUse : TextView = findViewById(R.id.termsOfUse)
        termsOfUse.text = policy
        termsOfUse.movementMethod = LinkMovementMethod.getInstance()

        clickEvents()
    }

    private fun clickEvents() {
        signUp_Btn.setOnClickListener {
            if (validateFields().first) {
                createBusinessOwner()
            } else {
                Toast.makeText(this, validateFields().second, Toast.LENGTH_SHORT).show()
            }
        }
    }

    private fun createBusinessOwner() {
        pDialog = ProgressDialog.show(this, null, "Signing up...", false, false)
        val mAuth = FirebaseAuth.getInstance()

        mAuth.createUserWithEmailAndPassword(email.text.toString(), password.text.toString()).addOnCompleteListener { task ->
            if (task.isSuccessful) {
                val newUserUID = mAuth.currentUser?.uid ?: ""
                createUserDataWith(id = newUserUID)
            } else {
                pDialog.dismiss()

                Toast.makeText(this, "Unable to create account.\n${task.exception?.localizedMessage}", Toast.LENGTH_SHORT).show()
            }
        }
    }

    private fun createUserDataWith(id: String) {

         userData = UserData(
                 key = id,
                 firstName = firstName.text.toString(),
                 lastName = lastName.text.toString(),
                 fullNmae = "${firstName.text} ${lastName.text}",
                 email = email.text.toString(),
                 phone = phone.text.toString(),
                 isGeneralUser = false,
                 verified = false,
                 meansOfID = meansOfIDSpinner.selectedItem as String,
                 idNumber = idNumber.text.toString(),
                 needsUpload = true)

        FirebaseDatabase.getInstance().reference.child("users").child(id).setValue(userData).addOnCompleteListener { task ->
            pDialog.dismiss()
            if (task.isSuccessful) {
                val dialog = AlertDialog.Builder(this)
                        .setTitle("One more step")
                        .setMessage("You need to upload a clear picture of your means of ID for verification. \nOnce this is verified successfully, you can proceed to add your businesses")
                        .setCancelable(false)
                        .setPositiveButton("CHOOSE PHOTO") { dialogInterface, _ ->
                            dialogInterface.dismiss()
                            chooseFromGallery()
                        }
                        .setNegativeButton("TAKE PHOTO") { dialogInterface, _ ->
                            dialogInterface.dismiss()
                            chooseFromCamera()
                        }.create()

                dialog.show()
            } else {
                Toast.makeText(this, "Unable to create account. -EO2", Toast.LENGTH_SHORT).show()
            }
        }
    }

    private fun chooseFromGallery() {
        val intent = Intent()
        intent.type = "image/*"
        intent.action = Intent.ACTION_GET_CONTENT
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICS_FROM_GALLERY)
    }

    private fun chooseFromCamera() {
        if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED ||
                ContextCompat.checkSelfPermission(this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED ||
                ContextCompat.checkSelfPermission(this, android.Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            //Request for permissions
            ActivityCompat.requestPermissions(this, arrayOf(android.Manifest.permission.CAMERA, android.Manifest.permission.WRITE_EXTERNAL_STORAGE, android.Manifest.permission.READ_EXTERNAL_STORAGE), REQUEST_CODE)
        } else {
            //Permissions granted
            capturePicture()
        }
    }

    private fun capturePicture() {
        val takePictureIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        if (takePictureIntent.resolveActivity(packageManager) != null) {
            startActivityForResult(takePictureIntent, PICS_FROM_CAMERA)
        }
    }

    private fun validateFields(): Pair<Boolean, String> {

        if (firstName.text.isBlank()) return false to "Please enter first name"
        if (lastName.text.isBlank()) return false to "Please enter last name"
        if (!Patterns.EMAIL_ADDRESS.matcher(email.text.toString()).matches()) return false to "Please enter a valid email address"
        if (phone.text.isBlank() or (phone.text.length < 11)) return false to "Please enter a valid phone number"
        if (password.text.isBlank() or (password.text.length < MINIMUM_CHARACTER_LENGTH)) return false to "Password must have a minimum of $MINIMUM_CHARACTER_LENGTH characters"
        if (password.text.toString() != confirmPassword.text.toString()) return false to "Passwords do not match"
        if (meansOfIDSpinner.selectedItemPosition == 0) return false to "Please select a valid means of identification"
        if (idNumber.text.isBlank()) return false to "Please enter your ID number"

        return true to ""
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        if (requestCode == REQUEST_CODE) {
            if (grantResults.isNotEmpty() and (grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                capturePicture()
            } else {
                Toast.makeText(this, "Err. We kinda need those permissions to proceed", Toast.LENGTH_SHORT).show()
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        when (requestCode) {
            PICS_FROM_CAMERA -> if (resultCode == Activity.RESULT_OK) uploadImageForVerification(mUri = data?.data)
            PICS_FROM_GALLERY -> if (resultCode == Activity.RESULT_OK) uploadImageForVerification(mUri = data?.data)
        }
    }

    private fun uploadImageForVerification(mUri: Uri?) {
        if (mUri != null){
            pDialog = ProgressDialog.show(this, null, "Uploading image for verification...", false, false)
            FirebaseStorage.getInstance().reference.child("verification_images/${userData.key}.jpg").putFile(mUri).addOnCompleteListener { task ->
                pDialog.dismiss()
                if (task.isSuccessful){
                    val newVerificationRequestKey = FirebaseDatabase.getInstance().reference.child("verification_requests").push().key

                    FirebaseDatabase.getInstance().reference.child("users").child(userData.key).child("needsUpload").setValue(false)
                    FirebaseDatabase.getInstance().reference.child("users").child(userData.key).child("declineMessage").setValue("Your ID is awaiting verification...")
                    FirebaseDatabase.getInstance().reference.child("verification_requests").child(newVerificationRequestKey).child("userName").setValue(userData.fullNmae)
                    FirebaseDatabase.getInstance().reference.child("verification_requests").child(newVerificationRequestKey).child("userID").setValue(userData.key)
                    FirebaseDatabase.getInstance().reference.child("verification_requests").child(newVerificationRequestKey).child("imageURL").setValue(task.result.downloadUrl.toString())

                    Toast.makeText(this, "Image upload was successful.", Toast.LENGTH_SHORT).show()

                    BaseUtil(this).let {
                        it.isLoggedIn = true
                        it.loggedInUser = userData
                    }

                    val intent = Intent(self@this, Home::class.java)
                    intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK
                    startActivity(intent)
                    finish()

                }else{
                    Toast.makeText(this, "Image upload was NOT successful", Toast.LENGTH_SHORT).show()
                    FirebaseDatabase.getInstance().reference.child("users").child(userData.key).child("declineMessage").setValue("Please upload your ID")

                    userData.needsUpload = true
                    BaseUtil(this).let {
                        it.isLoggedIn = true
                        it.loggedInUser = userData
                    }

                    val intent = Intent(this, Home::class.java)
                    intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK
                    startActivity(intent)
                    finish()
                }
            }
        }
    }
}

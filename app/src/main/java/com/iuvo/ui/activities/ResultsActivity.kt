package com.iuvo.ui.activities

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Location
import android.location.LocationManager
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.provider.Settings
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.support.v7.app.AlertDialog
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.View
import android.view.WindowManager
import android.widget.Toast
import com.firebase.geofire.GeoFire
import com.firebase.geofire.GeoLocation
import com.firebase.geofire.GeoQueryEventListener
import com.github.kittinunf.fuel.android.extension.responseJson
import com.github.kittinunf.fuel.httpGet
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.location.LocationListener
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.model.LatLng
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.iuvo.R
import com.iuvo.adapters.PlacesResultAdapter
import com.iuvo.models.ItemData
import com.iuvo.models.Result
import com.iuvo.utils.BaseUtil
import com.iuvo.utils.Store
import kotlinx.android.synthetic.main.activity_results.*
import org.jetbrains.anko.toast
import org.json.JSONException

class ResultsActivity : AppCompatActivity(), GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, LocationListener {

    private val permissionRequestCode = 1
    private val radius = 1200

    private lateinit var context : Context
    private lateinit var mGoogleApiClient: GoogleApiClient
    private lateinit var mLocationRequest: LocationRequest
    private var currentLatitude = 0.0
    private var currentLongitude = 0.0
    private lateinit var selectedItem : ItemData

    private var keys : MutableList<String> = mutableListOf()
    private var results : MutableList<Result> = mutableListOf()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN)
        setContentView(R.layout.activity_results)

        context = this

        selectedItem = Store.item
        toolbar.setToolbarTitle(selectedItem.name)

        buildGoogleApiClient()

        clickEvents()

    }


    private fun clickEvents() {

        connectionBtn.setOnClickListener {
            val intent = intent
            startActivity(intent)
            finish()
        }

        permissionBtn.setOnClickListener {
            connectionBtn.performClick()
        }

    }

    @SuppressLint("MissingPermission")
    private fun runQuery() {
        if(selectedItem.isFromGoogle){
            //Get from Google Places
            val url = getAppropriateUrl()
            url.httpGet().responseJson { _, _, result ->

                val(res, err) = result
                if (err != null){
                    noConnection.visibility = View.VISIBLE
                    return@responseJson
                }

                try {
                    val baseObject = res?.obj()
                    val resultsArray = baseObject?.getJSONArray("results") ?: ""

                    val type = object  : TypeToken<MutableList<Result>>(){}.type
                    results = Gson().fromJson(resultsArray.toString(), type)

                    if (results.isEmpty()){
                        toast("No match found for ${selectedItem.name}.")
                        finish()
                    }else{
                        showPlaces()
                    }

                }catch (e : JSONException){
                    Toast.makeText(context, e.localizedMessage, Toast.LENGTH_SHORT).show()
                }
            }
        }else{
            //Get from Firebase
            val geoFire = GeoFire(FirebaseDatabase.getInstance().reference.child("geofire").child(BaseUtil(self@this).makeFirebasePathFriendly(Store.item.name)))
            geoFire.queryAtLocation(GeoLocation(currentLatitude, currentLongitude),30.6).addGeoQueryEventListener(queryListener)
        }
    }

    private fun showPlaces() {

        loader.visibility = View.INVISIBLE

        val layoutManager = LinearLayoutManager(this@ResultsActivity)
        list.layoutManager = layoutManager

        val adapter = PlacesResultAdapter(this@ResultsActivity, results, LatLng(currentLatitude, currentLongitude), selectedItem)
        list.adapter = adapter
    }

    private fun getAppropriateUrl(): String {
        val stringBuilder = StringBuilder("https://maps.googleapis.com/maps/api/place/nearbysearch/json?sensor=true")
        stringBuilder.append("&key=${getString(R.string.API_KEY)}")
        stringBuilder.append("&type=${selectedItem.type}")
        stringBuilder.append("&location=$currentLatitude,$currentLongitude")
        stringBuilder.append("&radius=$radius")

        return stringBuilder.toString()
    }

    private val queryListener = object  : GeoQueryEventListener {

        override fun onGeoQueryReady() {
            if(keys.isNotEmpty()){
                //Get details
                getProviderDetails(keys)
            }else{
                toast("No match found for ${selectedItem.name}.")
                finish()
            }
        }

        override fun onKeyEntered(key: String?, location: GeoLocation?) {
            keys.add(key!!)
        }

        override fun onKeyMoved(key: String?, location: GeoLocation?) {
        }

        override fun onKeyExited(key: String?) {
            keys.remove(key!!)
        }

        override fun onGeoQueryError(error: DatabaseError?) {
            toast("${error?.message}")
            finish()
        }

    }

    private fun getProviderDetails(keys: MutableList<String>) {
        results.clear()
        keys.forEach {
            FirebaseDatabase.getInstance().reference.child("businesses").child(BaseUtil(self@this).makeFirebasePathFriendly(Store.item.name)).child(it).addValueEventListener( object  : ValueEventListener {
                override fun onCancelled(databaseError: DatabaseError?) {
                    toast(databaseError?.message ?: "Database error")
                    finish()
                }
                override fun onDataChange(dataSnapshot: DataSnapshot?) {
                    if(dataSnapshot!!.exists()){

                        val data = dataSnapshot.getValue(Result::class.java)!!
                        results.add(data)

                        if(results.size == keys.size){
                            showPlaces()
                        }
                    }else{
                        Log.d("DATA: ", "no data")
                    }
                }

            })
        }
    }

    override fun onResume() {
        super.onResume()
        checkLocationServiceStatus()
    }

    private fun checkLocationServiceStatus() {
        val mlocManager = this.getSystemService(Context.LOCATION_SERVICE) as LocationManager
        val enabled = mlocManager.isProviderEnabled(LocationManager.GPS_PROVIDER)

        if (!enabled) {
            showDialogGPS()
        }
    }

    private fun showDialogGPS() {
        val builder = AlertDialog.Builder(this)
        builder.setCancelable(false)
        builder.setTitle("Location services disabled")
        builder.setMessage("\nPlease enable GPS")
        builder.setPositiveButton("Enable") { dialog, _ ->
            dialog.dismiss()
            startActivity(Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS))
        }
        builder.setNegativeButton("Ignore") { dialog, _ ->
            dialog.dismiss()
            Toast.makeText(context, "Please turn on GPS to continue", Toast.LENGTH_SHORT).show()
            finish()
        }
        val alert = builder.create()
        alert.show()
    }

    private fun buildGoogleApiClient() {
        mGoogleApiClient = GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build()
        mGoogleApiClient.connect()
    }

    override fun onConnected(bundle: Bundle?) {
        getLocation()
    }

    private fun getLocation() {


        if (BaseUtil(this@ResultsActivity).hasCustomLocationSet){
            //Use previously saved location

            val savedLocation = BaseUtil(this@ResultsActivity).savedLocation

            currentLatitude = savedLocation.lat
            currentLongitude = savedLocation.lng

            runQuery()

        }else{
            //Use current location of user
            mLocationRequest = LocationRequest()
            mLocationRequest.smallestDisplacement = 20F
            mLocationRequest.priority = LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this)
            } else {
                Log.e("LOCATION", "Location permission not granted")
                ActivityCompat.requestPermissions(this, arrayOf(android.Manifest.permission.ACCESS_FINE_LOCATION, android.Manifest.permission.ACCESS_COARSE_LOCATION), permissionRequestCode)
                noPermision.visibility = View.VISIBLE
            }
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if(requestCode == permissionRequestCode){
            if (grantResults.isNotEmpty() && grantResults[0] == Activity.RESULT_OK){
                getLocation()
            }
        }

    }

    override fun onConnectionSuspended(i: Int) {
        noConnection.visibility = View.VISIBLE
    }

    override fun onConnectionFailed(p0: ConnectionResult) {
        noConnection.visibility = View.VISIBLE
    }

    override fun onLocationChanged(location: Location?) {

        currentLatitude = location?.latitude ?: 0.0
        currentLongitude = location?.longitude ?: 0.0

        runQuery()

        LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, LocationListener { _ ->
            Log.d("DEBUG", "REMOVE UPDATES")
        })


    }
}

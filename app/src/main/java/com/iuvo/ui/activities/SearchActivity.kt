package com.iuvo.ui.activities

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.GridLayoutManager
import android.text.Editable
import android.text.TextWatcher
import android.view.WindowManager
import com.iuvo.R
import com.iuvo.adapters.ItemAdapter
import com.iuvo.models.ItemData
import com.iuvo.utils.DataUtils
import kotlinx.android.synthetic.main.activity_search.*

class SearchActivity : AppCompatActivity() {

    private var allItems : MutableList<ItemData> = mutableListOf()
    private var filteredItems : MutableList<ItemData> = mutableListOf()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN)
        setContentView(R.layout.activity_search)

        allItems.addAll(DataUtils.getServices())
        allItems.addAll(DataUtils.getRepairs())
        allItems.addAll(DataUtils.getSales())

        searchContainer.addTextChangedListener(object : TextWatcher{
            override fun afterTextChanged(text: Editable?) {
                filterItems(text.toString())
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }
        })

        clickEvents()


    }

    private fun filterItems(text: String) {
        if (text.isNotBlank()){
            filteredItems.clear()
            filteredItems = allItems.filter { it.name.contains(text, true) }.toMutableList()
            showItems()

        }else{
            filteredItems.clear()
            showItems()
        }
    }

    private fun clickEvents() {
        backBtn.setOnClickListener {
            onBackPressed()
        }
    }

    private fun showItems(){
        val layoutManager = GridLayoutManager(this@SearchActivity,3)
        list.layoutManager = layoutManager
        val adapter = ItemAdapter(this@SearchActivity, filteredItems)
        list.adapter = adapter
    }
}

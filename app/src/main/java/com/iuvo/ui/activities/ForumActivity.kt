package com.iuvo.ui.activities

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import android.view.WindowManager
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import com.iuvo.R
import com.iuvo.adapters.ForumListAdapter
import com.iuvo.models.UserData
import com.iuvo.utils.BaseUtil
import kotlinx.android.synthetic.main.activity_forum.*

class ForumActivity : AppCompatActivity() {

    private val loggedInUser : UserData
        get() = BaseUtil(this).loggedInUser

    private var forumNames : MutableList<String> = mutableListOf()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN)
        setContentView(R.layout.activity_forum)

        getForums()
    }

    private fun getForums() {
        val forumsRef = FirebaseDatabase.getInstance().reference.child("users").child(loggedInUser.key).child("forums")

        forumsRef.addValueEventListener(object : ValueEventListener {
            override fun onCancelled(p0: DatabaseError?) {
                placeHolderText.text = "Unable to retrieve forums"
            }

            override fun onDataChange(snapShot: DataSnapshot?) {
                if (snapShot?.exists() == true){
                    snapShot.children.forEach {
                        forumNames.add(it.key)
                    }
                    showForums()
                }else{
                    placeHolderText.text = "You have no forums"
                }
            }
        })
    }

    private fun showForums() {
        if (forumNames.isNotEmpty()){
            val layoutManager = LinearLayoutManager(this)
            forumsList.layoutManager = layoutManager

            val adapter = ForumListAdapter(this, forumNames)
            forumsList.adapter = adapter

            placeHolder.visibility = View.GONE
        }
    }
}

package com.iuvo.ui.activities

import android.content.Context
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.WindowManager
import android.widget.Toast
import com.iuvo.R
import com.iuvo.adapters.ItemAdapter
import com.iuvo.models.ItemData
import com.iuvo.utils.DataUtils
import com.iuvo.utils.Store
import kotlinx.android.synthetic.main.activity_repairs.*

class RepairsActivity : AppCompatActivity() {

    lateinit var context : Context


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN)
        setContentView(R.layout.activity_repairs)

        context = this

        clickEvents()

        val layoutmanager = GridLayoutManager(context,3)
        list.layoutManager = layoutmanager
        val adapter = ItemAdapter(context, DataUtils.getRepairs())
        list.adapter = adapter
    }

    private fun clickEvents() {
        backBtn.setOnClickListener {
            onBackPressed()
        }

        searchContainer.setOnClickListener {
            val intent = Intent(this, SearchActivity::class.java)
            startActivity(intent)
        }
    }

}

package com.iuvo.ui.activities

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.WindowManager
import com.google.android.gms.common.GooglePlayServicesNotAvailableException
import com.google.android.gms.location.places.AutocompleteFilter
import com.google.android.gms.location.places.ui.PlaceAutocomplete
import com.iuvo.R
import com.iuvo.models.Location
import com.iuvo.models.UserData
import com.iuvo.utils.BaseUtil
import kotlinx.android.synthetic.main.activity_settings.*
import org.jetbrains.anko.selector
import org.jetbrains.anko.toast

class SettingsActivity : AppCompatActivity() {

    companion object {
        const val ADDRESS_REQUEST = 10
    }

    private val loggedInUser : UserData
        get() = BaseUtil(this@SettingsActivity).loggedInUser

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN)
        setContentView(R.layout.activity_settings)

        toolbar.setMinMode(!loggedInUser.isGeneralUser)

        showSavedData()

        clickEvents()

    }

    private fun showSavedData() {
        if (BaseUtil(this@SettingsActivity).hasCustomLocationSet){
            addressBtn.text = BaseUtil(this@SettingsActivity).customLocationAddress
        }else{
            addressBtn.text = "Use current location always"
        }
    }

    private fun clickEvents() {
        addressBtn.setOnClickListener {
            selector("Set location", listOf("Use current location always", "Choose custom location")) { dialogInterface, i ->
                dialogInterface.dismiss()
                when(i){
                    0 -> {
                        BaseUtil(this@SettingsActivity).hasCustomLocationSet = false
                        addressBtn.text = "Use current location always"
                    }
                    1 -> popUpGooglePlaces()
                }
            }
        }
    }

    private fun popUpGooglePlaces() {
        try {
            val intent = PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_OVERLAY)
                    .setFilter(AutocompleteFilter.Builder().setCountry("NG").build())
                    .build(this)
            startActivityForResult(intent, AddBusinessActivity.ADDRESS_REQUEST)
        }catch (e : GooglePlayServicesNotAvailableException){
            e.printStackTrace()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == AddBusinessActivity.ADDRESS_REQUEST) {
            when (resultCode) {
                Activity.RESULT_OK -> {
                    val place = PlaceAutocomplete.getPlace(this, data)

                    val location = Location()
                    location.lat = place.latLng.latitude
                    location.lng = place.latLng.longitude

                    BaseUtil(this@SettingsActivity).hasCustomLocationSet = true
                    BaseUtil(this@SettingsActivity).customLocationAddress = place.address.toString()
                    BaseUtil(this@SettingsActivity).savedLocation = location

                    addressBtn.text = place.address.toString()
                }
                PlaceAutocomplete.RESULT_ERROR -> {
                    val status = PlaceAutocomplete.getStatus(this, data)
                    toast(status.statusMessage ?: "Unable to get place")
                }
                Activity.RESULT_CANCELED -> toast("Action cancelled")
            }
        }
    }
}

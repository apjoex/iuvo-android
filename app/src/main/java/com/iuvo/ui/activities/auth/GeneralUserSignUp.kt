@file:Suppress("DEPRECATION")

package com.iuvo.ui.activities.auth

import android.app.ProgressDialog
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.text.Html
import android.text.method.LinkMovementMethod
import android.util.Patterns
import android.view.WindowManager
import android.widget.TextView
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.FirebaseDatabase
import com.iuvo.R
import com.iuvo.models.UserData
import com.iuvo.ui.activities.Home
import com.iuvo.utils.BaseUtil
import kotlinx.android.synthetic.main.activity_general_user_sign_up.*

class GeneralUserSignUp : AppCompatActivity() {

    private companion object {
        private const val MINIMUM_CHARACTER_LENGTH = 8
    }
    private lateinit var pDialog : ProgressDialog

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN)
        setContentView(R.layout.activity_general_user_sign_up)

        val policy = Html.fromHtml(getString(R.string.agree_terms_privacy))
        val termsOfUse : TextView = findViewById(R.id.termsOfUse)
        termsOfUse.text = policy
        termsOfUse.movementMethod = LinkMovementMethod.getInstance()

        clickEvents()
    }

    private fun clickEvents() {
        signUp_Btn.setOnClickListener {
            if(validateFields().first){
                createGeneralUser()
            }else{
                Toast.makeText(this, validateFields().second, Toast.LENGTH_SHORT).show()
            }
        }
    }

    private fun createGeneralUser() {
        pDialog = ProgressDialog.show(this, null, "Signing up...", false, false)
        val mAuth = FirebaseAuth.getInstance()
        mAuth.createUserWithEmailAndPassword(email.text.toString(), password.text.toString()).addOnCompleteListener { task ->
            if(task.isSuccessful){
                val newUserUID = mAuth.currentUser?.uid ?: ""
                createUserDataWith(id = newUserUID)
            }else{
                pDialog.dismiss()
                Toast.makeText(this, "Unable to create account.\n" +
                        "${task.exception?.localizedMessage}", Toast.LENGTH_SHORT).show()
            }
        }
    }

    private fun createUserDataWith(id: String) {

        val userData = UserData(
                key = id,
                firstName = firstName.text.toString(),
                lastName = lastName.text.toString(),
                fullNmae = "${firstName.text} ${lastName.text}",
                email = email.text.toString(),
                phone = phone.text.toString(),
                isGeneralUser = true)

       FirebaseDatabase.getInstance().reference.child("users").child(id).setValue(userData).addOnCompleteListener { task ->
           pDialog.dismiss()
           if(task.isSuccessful){
               BaseUtil(this).apply {
                   isLoggedIn = true
                   loggedInUser = userData
               }

               val intent = Intent(this, Home::class.java)
               intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK
               startActivity(intent)
               finish()
            }else{
               Toast.makeText(this, "Unable to create account. \n${task.exception?.localizedMessage}", Toast.LENGTH_SHORT).show()
           }
        }
    }

    private fun validateFields(): Pair<Boolean, String> {
        if (firstName.text.isBlank()) return false to "Please enter first name"
        if (lastName.text.isBlank()) return false to "Please enter last name"
        if (!Patterns.EMAIL_ADDRESS.matcher(email.text.toString()).matches()) return false to "Please enter a valid email address"
        if (phone.text.isBlank() or (phone.text.length < 11)) return false to "Please enter a valid phone number"
        if (password.text.isBlank() or (password.text.length < MINIMUM_CHARACTER_LENGTH)) return false to "Password must have a minimum of $MINIMUM_CHARACTER_LENGTH characters"
        if (password.text.toString() != confirmPassword.text.toString()) return false to "Passwords do not match"
        return true to ""
    }
}

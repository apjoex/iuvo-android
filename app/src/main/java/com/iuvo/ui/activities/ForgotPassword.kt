package com.iuvo.ui.activities

import android.app.ProgressDialog
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Patterns
import android.view.View
import android.view.WindowManager
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import com.iuvo.R
import kotlinx.android.synthetic.main.activity_forgot_password.*

class ForgotPassword : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN)
        setContentView(R.layout.activity_forgot_password)

        supportActionBar?.elevation = 0F
        clickEvents()
    }

    private fun clickEvents() {
        resetBtn.setOnClickListener {

            if (Patterns.EMAIL_ADDRESS.matcher(email.text.toString()).matches()){

                val pDialog = ProgressDialog.show(self@this, null, "Resetting password...", false, false)
                FirebaseAuth.getInstance().sendPasswordResetEmail(email.text.toString()).addOnCompleteListener { task ->
                    pDialog.dismiss()
                    if (task.isSuccessful){
                        successView.visibility = View.VISIBLE
                    }else{
                        Toast.makeText(self@this, "Unable to complete. Please try again", Toast.LENGTH_SHORT).show()
                    }
                }
            }else{
                Toast.makeText(self@this, "Please enter a valid email address", Toast.LENGTH_SHORT).show()
            }

        }
    }
}

package com.iuvo.ui.activities

import android.app.Activity
import android.app.ProgressDialog
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.view.WindowManager
import android.widget.Toast
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.storage.FirebaseStorage
import com.iuvo.R
import com.iuvo.models.UserData
import com.iuvo.utils.BaseUtil
import com.yalantis.ucrop.UCrop
import kotlinx.android.synthetic.main.activity_profile.*
import org.jetbrains.anko.alert
import org.jetbrains.anko.okButton
import org.jetbrains.anko.selector
import org.jetbrains.anko.toast
import java.io.File

class ProfileActivity : AppCompatActivity() {

    companion object {
        private const val REQUEST_CODE: Int = 100
        private const val PICS_FROM_CAMERA: Int = 101
        private const val PICS_FROM_GALLERY: Int = 102
    }

    private lateinit var loggedInUser : UserData
    private lateinit var userRef : DatabaseReference
    private var destinationUri : Uri  = Uri.EMPTY
    private var uriName = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN)
        setContentView(R.layout.activity_profile)

        loggedInUser = BaseUtil(this@ProfileActivity).loggedInUser

        toolbar.setMinMode(!loggedInUser.isGeneralUser)

        userRef = FirebaseDatabase.getInstance().reference.child("users").child(loggedInUser.key)

        enableAllFields(false)
        updateUI()

        clickEvents()
    }

    private fun clickEvents() {
        actionBtn.setOnClickListener {
            if (actionBtn.text.contains("Edit")){
                emailInput.visibility = View.GONE
                editPicBtn.visibility = View.VISIBLE
                enableAllFields(true)
                actionBtn.text = "Save profile"
            }else if(actionBtn.text.contains("Save")){
                saveChanges()
            }
        }

        editPicBtn.setOnClickListener {
            selector("Set Avatar", listOf("Choose photo","Take photo")) { dialogInterface, i ->
                dialogInterface.dismiss()
                if (i == 0){
                    chooseFromGallery()
                }else{
                    chooseFromCamera()
                }
            }

        }
    }

    private fun chooseFromGallery() {
        val intent = Intent()
        intent.type = "image/*"
        intent.action = Intent.ACTION_GET_CONTENT
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICS_FROM_GALLERY)
    }

    private fun chooseFromCamera() {
        if (ContextCompat.checkSelfPermission(self@ this, android.Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED ||
                ContextCompat.checkSelfPermission(self@ this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED ||
                ContextCompat.checkSelfPermission(self@ this, android.Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            //Request for permissions
            ActivityCompat.requestPermissions(self@ this, arrayOf(android.Manifest.permission.CAMERA, android.Manifest.permission.WRITE_EXTERNAL_STORAGE, android.Manifest.permission.READ_EXTERNAL_STORAGE), REQUEST_CODE)
        } else {
            //Permissions granted
            capturePicture()
        }
    }

    private fun capturePicture() {
        val takePictureIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        if (takePictureIntent.resolveActivity(packageManager) != null) {
            startActivityForResult(takePictureIntent, PICS_FROM_CAMERA)
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        if (requestCode == REQUEST_CODE) {
            if (grantResults.isNotEmpty() and (grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                capturePicture()
            } else {
                Toast.makeText(self@ this, "Err. We kinda need those permissions to proceed", Toast.LENGTH_SHORT).show()
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        when (requestCode) {
            PICS_FROM_CAMERA -> if (resultCode == Activity.RESULT_OK) cropImage(mUri = data?.data)
            PICS_FROM_GALLERY -> if (resultCode == Activity.RESULT_OK) cropImage(mUri = data?.data)
            69 -> {
                if (resultCode == -1){
                    destinationUri = UCrop.getOutput(data!!)!!
                    avatar.setImageURI(destinationUri)
                    avatar.requestLayout()
                }
            }
        }
    }

    private fun cropImage(mUri: Uri?) {

        val options = UCrop.Options()
        options.setToolbarColor(resources.getColor(R.color.colorCard))
        options.setStatusBarColor(resources.getColor(R.color.colorCard))

        uriName = "${System.currentTimeMillis()}.jpg"
        UCrop.of(mUri!!, Uri.fromFile(File(cacheDir, uriName)))
                .withOptions(options)
                .withAspectRatio(1F, 1F)
                .withMaxResultSize(300, 300)
                .start(this);
    }

    private fun saveChanges() {
        if (validateFields()){

            if (destinationUri != Uri.EMPTY){
                //Upload new avatar
                val pDialog = ProgressDialog.show(self@ this, null, "Uploading avatar...", false, false)
                FirebaseStorage.getInstance().reference.child("avatar/${loggedInUser.key}.jpg").putFile(destinationUri).addOnCompleteListener { task ->
                    pDialog.dismiss()
                    if (task.isSuccessful){
                        val downloadURL = task.result.downloadUrl.toString()
                        val newProps = mapOf(
                                "firstName" to firstNameInput.editText?.text.toString(),
                                "lastName" to lastNameInput.editText?.text.toString(),
                                "phone" to phoneInput.editText?.text.toString(),
                                "fullNmae" to "${firstNameInput.editText?.text.toString()} ${lastNameInput.editText?.text.toString()}",
                                "profilePic" to downloadURL)

                        userRef.updateChildren(newProps).addOnCompleteListener { task ->
                            if (task.isSuccessful){
                                emailInput.visibility = View.VISIBLE
                                editPicBtn.visibility = View.GONE
                                enableAllFields(false)
                                actionBtn.text = "Edit profile"

                                loggedInUser.firstName = firstNameInput.editText?.text.toString()
                                loggedInUser.lastName = lastNameInput.editText?.text.toString()
                                loggedInUser.phone = phoneInput.editText?.text.toString()
                                loggedInUser.fullNmae = "${loggedInUser.firstName} ${loggedInUser.lastName}"
                                loggedInUser.profilePic = downloadURL

                                BaseUtil(this@ProfileActivity).loggedInUser = loggedInUser
                                updateUI()
                            }
                        }

                    }else{
                        toast("Avatar was not uploaded. Please try again")
                    }
                }

            }else{
                val newProps = mapOf(
                        "firstName" to firstNameInput.editText?.text.toString(),
                        "lastName" to lastNameInput.editText?.text.toString(),
                        "fullNmae" to "${firstNameInput.editText?.text.toString()} ${lastNameInput.editText?.text.toString()}",
                        "phone" to phoneInput.editText?.text.toString())

                userRef.updateChildren(newProps).addOnCompleteListener { task ->
                    if (task.isSuccessful){
                        emailInput.visibility = View.VISIBLE
                        editPicBtn.visibility = View.GONE
                        enableAllFields(false)
                        actionBtn.text = "Edit profile"

                        loggedInUser.firstName = firstNameInput.editText?.text.toString()
                        loggedInUser.lastName = lastNameInput.editText?.text.toString()
                        loggedInUser.phone = phoneInput.editText?.text.toString()
                        loggedInUser.fullNmae = "${firstNameInput.editText?.text.toString()} ${lastNameInput.editText?.text.toString()}"

                        BaseUtil(this@ProfileActivity).loggedInUser = loggedInUser
                        updateUI()
                    }
                }
            }
        }
    }

    private fun validateFields(): Boolean {

        if (firstNameInput.editText?.text?.isBlank() == true){
            firstNameInput.editText?.error = "Please enter your first name"
            return false
        }

        if (lastNameInput.editText?.text?.isBlank() == true){
            lastNameInput.editText?.error = "Please enter your last name"
            return false
        }

        if (phoneInput.editText?.text?.length!! < 11){
            phoneInput.editText?.error = "Please enter a valid phone number"
            return false
        }

        return true
    }

    private fun enableAllFields(status: Boolean) {
        firstNameInput.isEnabled = status
        lastNameInput.isEnabled = status
        emailInput.isEnabled = status
        phoneInput.isEnabled = status
    }

    private fun updateUI() {
        firstNameInput.editText?.setText(loggedInUser.firstName)
        lastNameInput.editText?.setText(loggedInUser.lastName)
        emailInput.editText?.setText(loggedInUser.email)
        phoneInput.editText?.setText(loggedInUser.phone)

        Glide.with(this.applicationContext)
                .load(loggedInUser.profilePic)
                .apply(RequestOptions().placeholder(R.drawable.ic_profile_empty))
                .into(avatar)

    }
}

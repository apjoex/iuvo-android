package com.iuvo.ui.activities.onboarding

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter

class OnBoardingAdapter(fm: FragmentManager, private val details: List<OnBoardingData>): FragmentPagerAdapter(fm) {
    override fun getItem(position: Int): Fragment {
        val mFragment = OnBoardingFragment()
        val bundle = Bundle()
        bundle.putInt("drawable", details[position].img)
        bundle.putString("text", details[position].text)
        mFragment.arguments = bundle
        return mFragment
    }

    override fun getCount() = details.size
}

data class OnBoardingData (
        var text: String,
        var img: Int
)
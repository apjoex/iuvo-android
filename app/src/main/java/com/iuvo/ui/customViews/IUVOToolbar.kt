package com.iuvo.ui.customViews

import android.content.Context
import android.content.Intent
import android.graphics.Paint
import android.support.v7.app.AppCompatActivity
import android.text.TextPaint
import android.util.AttributeSet
import android.view.View
import android.widget.RelativeLayout
import com.iuvo.R
import com.iuvo.ui.activities.SearchActivity
import kotlinx.android.synthetic.main.layout_iuvo_toolbar.view.*

class IUVOToolbar : RelativeLayout {

    private var title = ""
    private var minMode = false

    constructor(context: Context) : super(context) {
        init(null, 0)
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        init(attrs, 0)
    }

    constructor(context: Context, attrs: AttributeSet, defStyle: Int) : super(context, attrs, defStyle) {
        init(attrs, defStyle)
    }

    private fun init(attrs: AttributeSet?, defStyle: Int) {
        // Load attributes
        val a = context?.obtainStyledAttributes(attrs, R.styleable.IUVOToolbar, defStyle, 0)
        if (a != null) {
            title = a.getString(R.styleable.IUVOToolbar_title)
            minMode = a.getBoolean(R.styleable.IUVOToolbar_minMode, false)
            a.recycle()
        }

        View.inflate(context, R.layout.layout_iuvo_toolbar, this)
    }

    override fun onFinishInflate() {
        super.onFinishInflate()

        toolbarTitle.text = title

        setMinMode(minMode)

        backBtn.setOnClickListener {
            val activity = context as AppCompatActivity
            activity.onBackPressed()
        }

        searchContainer.setOnClickListener {
            val activity = context as AppCompatActivity
            val intent = Intent(activity, SearchActivity::class.java)
            activity.startActivity(intent)
        }

    }

    fun setToolbarTitle(title : String){
        toolbarTitle.text = title
    }

    fun setMinMode(status : Boolean){
        if (status) {
            logoBackground.visibility = View.GONE
            searchContainer.visibility = View.GONE
        }else{
            logoBackground.visibility = View.VISIBLE
            searchContainer.visibility = View.VISIBLE
        }
    }



}
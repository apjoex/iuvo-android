package com.iuvo.models;

/**
 * Created by apjoex on 26/02/2018.
 */

public class ProviderData {

    private String name, address, description, phone, email_address, website;
    private Double latitude, longitude;
    private String thumbnail, key;

    public ProviderData() {
    }

    public ProviderData(String name, String address, String description, String phone, String email_address, String website, Double latitude, Double longitude, String thumbnail) {
        this.name = name;
        this.address = address;
        this.description = description;
        this.phone = phone;
        this.email_address = email_address;
        this.website = website;
        this.latitude = latitude;
        this.longitude = longitude;
        this.thumbnail = thumbnail;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail_address() {
        return email_address;
    }

    public void setEmail_address(String email_address) {
        this.email_address = email_address;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    @Override
    public String toString() {
        return name+"\n"+description;
    }
}

package com.iuvo.models

import java.io.Serializable
import java.util.*

/**
 * Created by apjoex on 25/02/2018.
 */
data class ItemData (
        var name : String,
        var type : String,
        var drawable : Int,
        var isFromGoogle : Boolean
){
    override fun toString(): String {
        return name
    }
}

data class Location (
        var lat : Double = 0.0,
        var lng : Double = 0.0
)

data class Geometry (
        var location : Location? = null
)

data class Photo (
        var photo_reference : String = ""
)

data class OpeningHour(
        var open_now : Boolean = true
)

data class Result  (
        var geometry: Geometry? = null,
        var icon : String = "",
        var id : String = "",
        var name : String = "",
        var opening_hours : OpeningHour? = null,
        var photos : MutableList<Photo>? = null,
        var place_id : String = "",
        var rating : Float? = null,
        var reference: String = "",
        var vicinity : String = "",
        var mail : String = "",
        var phone : String = "",
        var website : String = "",
        var customSource : Boolean = false,
        var ownerID : String = "",
        var ownerName : String = "",
        var category : String = ""
) : Serializable

data class UserData (
        var key : String = "",
        var firstName : String = "",
        var lastName : String = "",
        var fullNmae : String = "",
        var email : String = "",
        var phone : String = "",
        var isGeneralUser : Boolean = false,
        var verified : Boolean = false,
        var meansOfID : String = "",
        var idNumber : String = "",
        var needsUpload : Boolean = false,
        var declineMessage : String = "",
        var profilePic : String = ""
)

data class ChatMessage (
        var text : String = "",
        var senderID : String = "",
        var senderName : String = "Sample",
        var time: Long = Date().time
)

data class NotifData (
        var message : String,
        var receiverFCM : String,
        var senderName : String
)
package com.iuvo.services

import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.graphics.BitmapFactory
import android.os.Build
import android.util.Log
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import com.iuvo.BuildConfig
import com.iuvo.R
import android.app.PendingIntent
import android.content.Intent
import android.support.v4.app.NotificationCompat
import com.iuvo.ui.activities.Home


class FCMMessagingService : FirebaseMessagingService() {

    companion object {
        const val TAG = "FCM"
    }

    override fun onMessageReceived(remoteMessage: RemoteMessage?) {

        Log.d(TAG, "From: " + remoteMessage?.from)
        Log.d(TAG, "DATA_TITLE: " + remoteMessage?.data!!["title"])
        Log.d(TAG, "DATA: " + remoteMessage.data!!["body"])

        val intent = Intent(this, Home::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        val pendingIntent = PendingIntent.getActivity(this, 0, intent, 0)

        val notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val channel = NotificationChannel(BuildConfig.APPLICATION_ID,"Chat",NotificationManager.IMPORTANCE_HIGH)
            channel.description = "This channel notifies new chat messages"
            channel.enableVibration(true)
            notificationManager.createNotificationChannel(channel)
        }

        val notification = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            Notification.Builder(this)
                    .setContentTitle(remoteMessage.data["title"])
                    .setContentText(remoteMessage.data["body"])
                    .setChannelId(BuildConfig.APPLICATION_ID)
                    .setSmallIcon(R.mipmap.logo_white)
                    .setContentIntent(pendingIntent)
                    .setDefaults(Notification.DEFAULT_ALL)
                    .setLargeIcon(BitmapFactory.decodeResource(resources, R.mipmap.ic_launcher_round))
                    .setPriority(Notification.PRIORITY_HIGH)
                    .build()
        } else {
            Notification.Builder(this)
                    .setContentTitle(remoteMessage.data["title"])
                    .setContentText(remoteMessage.data["body"])
                    .setSmallIcon(R.mipmap.logo_white)
                    .setContentIntent(pendingIntent)
                    .setDefaults(Notification.DEFAULT_ALL)
                    .setLargeIcon(BitmapFactory.decodeResource(resources, R.mipmap.ic_launcher_round))
                    .setPriority(Notification.PRIORITY_HIGH)
                    .build()
        }

        notificationManager.notify(101, notification)

    }
}
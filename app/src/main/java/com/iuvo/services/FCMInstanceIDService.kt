package com.iuvo.services

import android.util.Log
import com.google.firebase.iid.FirebaseInstanceId
import com.google.firebase.iid.FirebaseInstanceIdService
import com.iuvo.utils.BaseUtil

class FCMInstanceIDService : FirebaseInstanceIdService() {

    override fun onTokenRefresh() {
        val refreshedToken = FirebaseInstanceId.getInstance().token
        saveToken(refreshedToken)
    }

    private fun saveToken(refreshedToken: String?) {
        //
        Log.d("FCM", refreshedToken)
        BaseUtil(this).fcmToken = refreshedToken ?: ""
    }
}